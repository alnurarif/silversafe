import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import logo from './logo.svg';
import logonew from './img/brand-logo.png';
import valuetimage from './img/vault.png';
import perfectimg1 from './img/perfect-img1.jpg';
import icon1 from './img/icon1.png';
import icon2 from './img/icon2.png';
import icon3 from './img/icon3.png';
import icon4 from './img/icon4.png';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import { Modal, Button } from 'react-bootstrap';
import { show } from 'react';
import { useState } from 'react';
import { render } from 'react';

//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

//Owl Carousel Settings
const options = {
	loop:true,
	center: true,
	margin:30,
	nav:false,
	dots:true,
	autoplay:false,
	autoplayHoverPause:true,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      600: {
          items: 2,
      },
      1000: {
          items: 3,

      }
  },
};

function App() {
  return (
    <div className="App">
      <header className="header-bg">
         <div className="container">
            <div className="row">
               <div id="heading-nav2" className="col-md-8 heading-nav">
			<Navbar expand="lg">
			  <Navbar.Brand href="/"><img src={logonew} className="site-logo" alt="logonew" /></Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  <Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
				  <Nav.Link className="active" href="#home">this is app</Nav.Link>
				  <Nav.Link href="#">Mortgage Refi</Nav.Link>
				  <Nav.Link href="#">About</Nav.Link>
				</Nav>
			  </Navbar.Collapse>
			</Navbar>
               </div>
			  <div className="col-md-4 heading-btn">
              <a href="#">Get Started</a>
              <a className="last" href="#">Log In</a>
               </div>
            </div>
         </div>
      </header>
      <section className="silver-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-6 text-silver">
               <h1>SilverSafe</h1>
               <p>The platform for your legacy planning 
and mortgage refinancing needs.</p>
                <a href="#"><button type="button">Get Started <i className="fa fa-arrow-right"></i></button></a>
               </div>
               <div className="col-lg-6 text-silver-img">
			   <img id="desktop-silver" src={valuetimage} className="site-logo" alt="valuetimage" />
               </div>
            </div>
         </div>
         <div className="container">
            <div className="row">
               <div className="col-lg-12 silver-social">
               <a href="#"><i className="fa fa-twitter"></i></a>
               <a href="#"><i className="fa fa-linkedin"></i></a>
               <a href="#"><i className="fa fa-google"></i></a>
               </div>
            </div>
         </div>
      </section>
      <section className="perfect-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-7 text-perfect">
               <h2>Perfect for will writing
				and mortgage refinancing</h2>
                <img src={perfectimg1} alt="img1" />
               </div>
               <div className="col-lg-5 text-perfect2">
               <p>Creating and maintaining a positive legacy for your loved ones has never been easier with the online drafting of wills and a range of mortgage brokers to choose from to lower your mortgage payments. </p>
               <div className="text-perfect3">
               <img src={icon1} alt="icon1" />
               <h3>Wills Managment</h3>
               <p>You can draft and store your wills
via our online platform.</p>
               </div>
               <div className="text-perfect3">
               <img src={icon2} alt="icon2" />
               <h3>Ease of Communication</h3>
               <p>A stress-free approach for the right experts and documents.</p>
               </div>
               <div className="text-perfect3">
               <img src={icon3} alt="icon3" />
               <h3>Mortgage Refinancing</h3>
               <p>You can get a cheaper loan at the right time effortlessly.</p>
               </div>
               </div>
            </div>
         </div>
      </section>
      <section className="reviews-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-12 reviews-heading">
               <span>Testimonials</span>
               <h2>What People Are Saying</h2>
               </div>
            </div>
         </div>
       <div className="container-fluid">
            <div className="row wow fadeIn" data-wow-delay="0.2s">
                <div className="col-md-12">
                    <div className="testimonial-list">
<OwlCarousel className="slider-items owl-carousel" {...options}>
                      <div className="item">
					  <div className="single-testimonial">
                            <div className="single-content">
                            <h2>A will and trust set up in under 5min!</h2>
                            <p>Extremely efficient service. This platform really helped educate me on the importance of legacy planning and allowed me to outsource the process of will writing so that I was better able to focus on catering to the needs of my family. I would recommend them to those who want to start legacy planning.</p>
							<div className="single-content-border"></div>
                            <img src={icon4} alt="icon4" />
                            <h3>David Mourinho</h3>
                            <h4>Director of Materials Management</h4>
                            </div>
                        </div>
						</div>
                      <div className="item">
					  <div className="single-testimonial">
                            <div className="single-content">
                            <h2>A will and trust set up in under 5min!</h2>
                            <p>Extremely efficient service. This platform really helped educate me on the importance of legacy planning and allowed me to outsource the process of will writing so that I was better able to focus on catering to the needs of my family. I would recommend them to those who want to start legacy planning.</p>
							<div className="single-content-border"></div>
                            <img src={icon4} alt="icon4" />
                            <h3>David Mourinho</h3>
                            <h4>Director of Materials Management</h4>
                            </div>
                        </div>
					  </div>
                      <div className="item">
					  <div className="single-testimonial">
                            <div className="single-content">
                            <h2>A will and trust set up in under 5min!</h2>
                            <p>Extremely efficient service. This platform really helped educate me on the importance of legacy planning and allowed me to outsource the process of will writing so that I was better able to focus on catering to the needs of my family. I would recommend them to those who want to start legacy planning.</p>
							<div className="single-content-border"></div>
                            <img src={icon4} alt="icon4" />
                            <h3>David Mourinho</h3>
                            <h4>Director of Materials Management</h4>
                            </div>
                        </div>
					  </div>
                      <div className="item">
					  <div className="single-testimonial">
                            <div className="single-content">
                            <h2>A will and trust set up in under 5min!</h2>
                            <p>Extremely efficient service. This platform really helped educate me on the importance of legacy planning and allowed me to outsource the process of will writing so that I was better able to focus on catering to the needs of my family. I would recommend them to those who want to start legacy planning.</p>
							<div className="single-content-border"></div>
                            <img src={icon4} alt="icon4" />
                            <h3>David Mourinho</h3>
                            <h4>Director of Materials Management</h4>
                            </div>
                        </div>
					  </div>
                  </OwlCarousel>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <section className="join-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-8 offset-2 join-heading">
               <h2>Join Our Newsletter</h2>
               <p>Get updated on news regarding the Intestate Succession Act, wills and trusts through our weekly newsletter!</p>
               </div>
            </div>
         </div>
         <div className="container">
         <div className="row">
         <div className="col-lg-8 offset-2 join-form">
        <form>
          <div className="form-row align-items-center">
            <div className="col-auto">
              <i className="fa fa-envelope-o" aria-hidden="true"></i><input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Type your email"></input>
            </div>
            <div className="col-auto">
              <button type="submit" className="btn btn-primary mb-2">Subscribe</button>
            </div>
          </div>
        </form>
         </div>
         </div>
         </div>
         <div className="container">
            <div className="row">
               <div className="col-lg-8 offset-2 join-heading2">
               <p>Join over 1000 happy customers!</p>
               </div>
            </div>
         </div>
      </section>
      <footer className="footer-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-3 footer-logo">
               <h3>SilverSafe</h3>
               </div>
               <div className="col-lg-6 footer-nav">
               <ul>
               <li><a href="#">About Us</a></li>
               <li><a href="#">Terms and Conditions</a></li>
               <li><a href="#">Privacy Policy</a></li>
               </ul>
               </div>
               <div className="col-lg-3 footer-social">
               <a href="#"><i className="fa fa-twitter"></i></a>
               <a href="#"><i className="fa fa-linkedin"></i></a>
               <a href="#"><i className="fa fa-google"></i></a>
               </div>
            </div>
         </div>
      </footer>
      <section className="copyright-section">
         <div className="container copyright-wrap">
            <div className="row">
               <div className="col-lg-12 copyright-text">
                  <p>Copyright © 2020 Normn Pte Ltd</p>
               </div>
            </div>
         </div>
      </section>  
	
	  
    </div>
  );
}

export default App;
