import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import logo from './logo.svg';
import logonew from './img/brand-logo.png';
import {
  Route,
  NavLink,
  BrowserRouter,
  Switch
} from "react-router-dom";
import Home from "./Home";
import legacy from "./legacy";
import mortgage from "./mortgage-refi";
import userportalacc from "./userportalacc";
import loginuser from "./loginuser";
import profile from "./profile";

 
class Main extends Component {
  render() {
    return (
      <BrowserRouter>
	  <Switch>
        <div>

	<div className="container-fluid main-container-wrap">
	<div className="row">
	<div className="col-lg-12">
   <div className="content">
  <Route exact path="/" component={Home}/>
  <Route path="/legacy" component={legacy}/>
  <Route path="/mortgage-refi" component={mortgage}/>
  <Route path="/userportalacc" component={userportalacc}/>
  <Route path="/loginuser" component={loginuser}/>
  <Route path="/profile" component={profile}/>
   </div>
   </div>
   </div>
   </div>
	  </div>
	  </Switch>
      </BrowserRouter>
	  
    );
  }
}
 
export default Main;
