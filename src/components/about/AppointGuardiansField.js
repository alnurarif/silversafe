import React, { Component } from "react";
import CountryList from "../CountryList.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class AppointGuardiansField extends Component {
	constructor(props){
		super(props);
		this.state = {
			all_state : this.props.allState,
			guardians_number : this.props.guardians_number
		}
	}
	plusMinusSymbol(){
		return false;
		let guardian_number = this.props.guardian_number;
		let guardians_count = this.props.allState.key_info.appointment_of_guardians.main.length;
		if(guardian_number == 1 && guardians_count<4){
			return (<button className="button_1 floatright" onClick={() => this.props.addGuardian()}>Add Guardian  <span><FontAwesomeIcon icon="plus"/></span></button>);
		}else if(guardian_number>1 && guardian_number==guardians_count){
			return (
					<img className="cursor_pointer" src={this.props.icon_thirteen_minus} alt="icon13" onClick={() => this.props.removeGuardian()}/>
			);
		}
	}
	guardianHeader(){
		let guardian_number = this.props.guardian_number;
		let guardians_count = this.props.allState.key_info.appointment_of_guardians.main.length;
		if(guardian_number == 1){
			return (<h3>MAIN GUARDIAN <img src={this.props.icon_twelve} alt="icon12"/></h3>);
		}else if(guardian_number == 2){
			return (<h3>SECOND GUARDIAN (OPTIONAL) <img src={this.props.icon_twelve} alt="icon12"/></h3>);
		}else if(guardian_number == 3){
			return (<h3>THIRD GUARDIAN (OPTIONAL) <img src={this.props.icon_twelve} alt="icon12"/></h3>);
		}else if(guardian_number == 4){
			return (<h3>FORTH GUARDIAN (OPTIONAL) <img src={this.props.icon_twelve} alt="icon12"/></h3>);
		}
	}
	render(){
		let props = this.props;
		let key_info_two_errors_main = props.allState.key_info_two_errors.keyInfoTwoErrors;
		let currentMainComponentError;
		let allComponentsErrorsMain;
		if(!(Object.keys(key_info_two_errors_main).length === 0 && key_info_two_errors_main.constructor === Object)){
			allComponentsErrorsMain = key_info_two_errors_main.main;
			currentMainComponentError = allComponentsErrorsMain[props.guardian_number-1];
		}
		return (
			<React.Fragment>
				<div className="form-row justify-content-center">
					<div className="col-6">
						{this.guardianHeader()}
					</div>
					<div className="col-6 tab-plus-icon">
						{this.plusMinusSymbol()}
					</div>
					<div className="col-auto">
						<label>First Name</label>
						<input type="text" className={"form-control mb-2 "+(currentMainComponentError && currentMainComponentError.first_name==true ? 'error_border' : '')} id="inlineFormInput" name="first_name" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)} />
					</div>
					<div className="col-auto">
						<label>Last Name</label>
						<input type="text" className={"form-control mb-2 "+(currentMainComponentError && currentMainComponentError.last_name==true ? 'error_border' : '')} id="inlineFormInput" name="last_name" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)} />
					</div>
					<div className="col-auto">
						<label>NRIC (or Passport)</label>
						<input type="text" className={"form-control mb-2 "+(currentMainComponentError && currentMainComponentError.nric==true ? 'error_border' : '')} id="inlineFormInput" name="nric" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)} />
					</div>
					<div className="col-auto">
						<label>Country of Birth</label>
						<select className={(currentMainComponentError && currentMainComponentError.country_of_birth==true ? 'error_border' : '')} name="country_of_birth" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)}>
							<CountryList/>
						</select>
					</div>
					<div className="col-auto">
						<label>Country of Residence</label>
						<select className={(currentMainComponentError && currentMainComponentError.country_of_residence==true ? 'error_border' : '')} name="country_of_residence" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)}>
							<CountryList/>
						</select>
					</div>
					<div className="col-auto">
						<label>Gender</label>
						<select className={(currentMainComponentError && currentMainComponentError.gender==true ? 'error_border' : '')} name="gender" onChange={(e)=> this.props.setGuardianChangedValue(e.target,props.guardian_number)}>
							<option value="">Select</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
				</div>

				<div className="justify-content-center tab-profile2-hr"><hr/></div>
			</React.Fragment>
		);
	}
}
export default AppointGuardiansField;