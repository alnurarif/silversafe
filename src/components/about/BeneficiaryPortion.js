import React, { Component } from "react";
import Item from './Item.js';
class BeneficiaryPortion extends Component {
	constructor(props){
		super(props);
			this.state = {
			rangeValue : 50.00,
			gifted_sum : "",
		}
	}
	changeRange = (target) => {
		this.setState(
			{rangeValue : target.value}
		)
	}
	checkNumeric = (target) => {
		if(!isNaN(target.value)){
			this.setState(
				{
					gifted_sum : target.value
				}
			)
		}
	}
	render(){
		let props = this.props;
		let classes = (props.active_tab == 4) ? "tab-pane fade show active" : "tab-pane fade" ;
		let currentComponentError = props.allState.distribution_errors;
		return(
			<React.Fragment>
				<div className="col-auto">
					<label>First Name</label>
					<input type="text" className={"form-control mb-2 "+(currentComponentError && currentComponentError.first_name==true ? 'error_border' : '')} id="first_name" name="first_name" onChange={(e)=> this.props.setBenificiaryInput(e.target)}/>
				</div>
				<div className="col-auto">
					<label>Last Name</label>
					<input type="text" className={"form-control mb-2 "+(currentComponentError && currentComponentError.last_name==true ? 'error_border' : '')} id="last_name" name="last_name" onChange={(e)=> this.props.setBenificiaryInput(e.target)}/>
				</div>
				<div className="col-auto">
					<label>NRIC (or Passport)</label>
					<input type="text" className={"form-control mb-2 "+(currentComponentError && currentComponentError.nric==true ? 'error_border' : '')} id="nric" name="nric" onChange={(e)=> this.props.setBenificiaryInput(e.target)}/>
				</div>

				<div className="justify-content-center tab-profile3-hr"><hr/></div>
				<div id="slide-div" className="form-row justify-content-center">
					<div className="col-9 slideprogress">
						<label>Allocation of Estate (%)</label>
						<input type="range" className="form-control-range" min="0" max="100" step="0.01" id="allocation_of_state_range" name="allocation_of_state_range"  onChange={(e)=> {this.changeRange(e.target); this.props.setBenificiaryInput(e.target)}}/>
					</div>
					<div className="col-3 state-percent">
						<input id="allocation_of_state" type="text" className="form-control mb-2" name="allocation_of_state"  onChange={(e)=> this.props.setBenificiaryInput(e.target)} value={this.state.rangeValue} readOnly/>
					</div>
				</div>
				<div className="form-row distribute-checkboxdiv">
					<div className="col-12">
						<input type="checkbox" className={"form-check-input "+(this.props.allState.distribution_errors && this.props.allState.distribution_errors.beneficiary_checkbox_one_error==true ? 'error_outline' : '')} id="exampleCheck1" name="beneficiary_checkbox_one" onClick={(e)=> this.props.distributeCheckboxUpdate(e.target)}/>
						<label id="profilelabel" className="form-check-label" htmlFor="exampleCheck1">If any one of the abovenamed beneficiaries is not living at my death, I give his/her interest in my assets to the remaining surviving beneficiaries in equal shares.</label>
					</div>
				</div>
				<div className="justify-content-center tab-profile3-hr"><hr/></div>
				<div className="form-row justify-content-center">
					<div className="col-12">
						<h3>Allocation of all my interest in the Property known as (if any)</h3>
					</div>
					<div className="col-12">
						<label>Address</label>
						<input id="address" type="text" className={"form-control mb-2"} name="address" onChange={(e)=> this.props.setBenificiaryInput(e.target)}/>
					</div>
				</div>
				<div id="distribute-property" className="form-row distribute-checkboxdiv">
					<div className="col-12">
						<input type="checkbox" className={"form-check-input "+(this.props.allState.distribution_errors && this.props.allState.distribution_errors.beneficiary_checkbox_two_error==true ? 'error_outline' : '')} id="exampleCheck1" name="beneficiary_checkbox_two" onClick={(e)=> this.props.distributeCheckboxUpdate(e.target)}/>
						<label id="profilelabel" className="form-check-label" htmlFor="exampleCheck1">(i) I give the Property absolutely to the Property Donee. </label>
					</div>
				</div>
				<div className="form-row distribute-checkboxdiv">
					<div className="col-12">
						<input type="checkbox" className={"form-check-input "+(this.props.allState.distribution_errors && this.props.allState.distribution_errors.beneficiary_checkbox_three_error==true ? 'error_outline' : '')} id="exampleCheck1" name="beneficiary_checkbox_three" onClick={(e)=> this.props.distributeCheckboxUpdate(e.target)}/>
						<label id="profilelabel" className="form-check-label" htmlFor="exampleCheck1">(ii) My Trustees must pay from my Estate any mortgage debt or other sums secured on the Property (to the extent that they are not paid from the proceeds of a life assurance policy given as additional security for payment), and any related interest or expenses.] </label>
					</div>
				</div>
				<div className="justify-content-center tab-profile3-hr"><hr/></div>
				<div className="form-row gifts-div">
					<div className="col-12">
						<h3>Gifts of Money (if any)</h3>
					</div>
					<div className="col-12 valuesgd">
						<label>Gifted Sum (SGD)</label>
						<input type="text" className="form-control mb-2" id="gifted_sum" name="gifted_sum" onChange={(e)=> {this.props.setBenificiaryInput(e.target); this.checkNumeric(e.target);}} value={this.state.gifted_sum}/>
					</div>
				</div>
				<div className="form-row justify-content-center distri-additional">
					<div className="col-8">
						<h3>Other Personal Possessions (if any)</h3>
					</div>
					<div className="col-4 distri-add">
						<button className="button_1 floatright" onClick={()=>this.props.addItemToTemporaryDistribution()}>Add Item <i className="fa fa-plus"></i></button>
					</div>
					{this.props.allState.distribution.temporary_beneficiary.items.map((singleBeneficiaryItem,index)=>(
						<Item key={index} item_number={index} allState={this.props.allState} removeBeneficiaryItem={this.props.removeBeneficiaryItem} singleBeneficiaryItem={singleBeneficiaryItem} setBenificiaryItemInput={this.props.setBenificiaryItemInput}/>
					)) }
					<div className="col-12 distri-last">
						<button id="distri-btn2" type="submit" className="btn btn-primary mb-2" onClick={()=>this.props.checkDistributionValidation()}>Add Beneficary to Will</button>
					</div>
				</div>
			</React.Fragment>
		);
	}
}
export default BeneficiaryPortion;