import React, { Component } from "react";
class CharityPortion extends Component {
	constructor(props){
		super(props);
		this.state = (
			{
				charity_gifted_sum : ""		
			}
		)
	}
	checkNumericCharity = (target) => {
		if(!isNaN(target.value)){
			this.setState(
				{
					charity_gifted_sum : target.value
				}
			)
		}
	}
	render(){
		let props = this.props.allState;	
		return (
			<React.Fragment>
				<div className="col-auto">
					<label>Name of Charity</label>
					<input type="text" className={"form-control mb-2 "+(props.charity_errors && props.charity_errors.charity_name==true ? 'error_border' : '')} id="charity_name" name="charity_name" onChange={(e)=> this.props.setCharityInput(e.target)}/>
				</div>
				<div className="col-auto">
					<label>Address</label>
					<input type="text" className={"form-control mb-2 "+(props.charity_errors && props.charity_errors.charity_address==true ? 'error_border' : '')} id="charity_address" name="charity_address" onChange={(e)=> this.props.setCharityInput(e.target)}/>
				</div>
				<div className="col-12 column-sum">
					<label>Gifted Sum (SGD)</label>
					<input type="text" className={"form-control mb-2 "+(props.charity_errors && props.charity_errors.charity_gifted_sum==true ? 'error_border' : '')} id="charity_gifted_sum" name="charity_gifted_sum" onChange={(e)=> {this.props.setCharityInput(e.target); this.checkNumericCharity(e.target);}} value={this.state.charity_gifted_sum}/>
				</div>
				<div className="col-12 distri-last">
					<button id="distri-btn2" type="submit" className="btn btn-primary mb-2" onClick={()=>this.props.addCharityObjectToArray()}>Add Charity to Will</button>
				</div>
			</React.Fragment>
		);	
	}
	
}
export default CharityPortion;