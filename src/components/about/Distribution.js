import React, { Component } from "react";
import DistributionFirst from "./DistributionFirst.js";
import DistributionSecond from "./DistributionSecond.js";
import DistributionThird from "./DistributionThird.js";
class Distribution extends Component {
    constructor(props){
        super(props);
    }
    
    getCurrentSection(){
        if(this.props.allState.active_distribute_section == 1){
            return (<DistributionFirst 
                allState={this.props.allState} 
                icon_thirteen={this.props.icon_thirteen} 
                icon_forteen={this.props.icon_forteen} 
                changeDistributionSection={this.props.changeDistributionSection} 
                icon_fifteen={this.props.icon_fifteen}
                removeBeneficiary={this.props.removeBeneficiary}
                removeCharity={this.props.removeCharity}
                setDistributionOneCheckbox={this.props.setDistributionOneCheckbox}
                validateAndGo={this.props.validateAndGo}
                goToPreviewPage={this.props.goToPreviewPage}/>
            ); 
        }else if(this.props.allState.active_distribute_section== 2){
            return (<DistributionSecond 
                allState={this.props.allState} 
                icon_forteen={this.props.icon_forteen} 
                progress_slider={this.props.progress_slider} 
                changeDistributionSection={this.props.changeDistributionSection} 
                setBenificiaryInput={this.props.setBenificiaryInput} 
                addBeneficiaryObjectToArray={this.props.addBeneficiaryObjectToArray}
                addCharityObjectToArray={this.props.addCharityObjectToArray}
                setBeneficiaryType={this.props.setBeneficiaryType}
                checkDistributionValidation={this.props.checkDistributionValidation}
                distributeCheckboxUpdate={this.props.distributeCheckboxUpdate}
                setBenificiaryItemInput={this.props.setBenificiaryItemInput}
                setCharityInput={this.props.setCharityInput}
                removeBeneficiaryItem={this.props.removeBeneficiaryItem}
                addItemToTemporaryDistribution={this.props.addItemToTemporaryDistribution}
                setDistributionOneCheckbox = {this.props.setDistributionOneCheckbox}/>
            );
        }else if(this.props.allState.active_distribute_section== 3){
            return (<DistributionThird 
                allState={this.props.allState} 
                icon_thirteen={this.props.icon_thirteen} 
                icon_fifteen={this.props.icon_fifteen}/>
            );
        }
    }
    render(){
        let props = this.props;
        let classes = (props.active_tab == 5) ? "tab-pane fade show active" : "tab-pane fade" ;
        return (
            <div className={classes} id="distribution" role="distribution" aria-labelledby="distribution">
                <div className="container">
                    <div className="row">
                        {this.getCurrentSection()}
                    </div>
                </div>
            </div>
        );
    }
}
export default Distribution;