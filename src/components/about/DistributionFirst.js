import React, { Component } from "react";
import SingleBeneficiary from "./SingleBeneficiary.js";
import SingleCharity from "./SingleCharity.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class DistributionFirst extends Component {
    constructor(props){
        super(props);
        this.state = {
            beneficiaries : this.props.allState.distribution.beneficiaries
        }
    }
    showOrNotShowBeneficiaryMessage(){
        if(this.props.allState.distribution.beneficiaries.length == 0){
            return (<p>You don’t have any beneficiaries in your draft will as yet.</p>);
        }
    }
    showOrNotShowCharityMessage(){
        if(this.props.allState.distribution.charities.length == 0){
            return (<p>You don’t have any charities in your draft will.</p>);
        }
    }
    render(){
        let props = this.props;
        let classes = (props.active_tab == 4) ? "tab-pane fade show active" : "tab-pane fade" ;
           
        return (
            <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
                <h2>Distribution of Estate / Gifts of Money / Gifts of Charity</h2>
                <p className="paragraph">You can name the beneficiaries / charities of your will here:</p>
                <div className="form-row justify-content-center">
                    <div className="col-8">
                        <h3>BENEFICIARIES</h3>
                        {this.showOrNotShowBeneficiaryMessage()}
                    </div>
                    <div className="col-4 tab-plus-icon">
                        <button className="button_1 floatright" onClick={() => props.changeDistributionSection(2)}>Add <span><FontAwesomeIcon icon="plus"/></span></button>
                    </div>
                </div>
                {this.props.allState.distribution.beneficiaries.map((singleBeneficiary,index)=>(
                    <SingleBeneficiary 
                        key={index} 
                        beneficiary_number={index} 
                        icon_fifteen={this.props.icon_fifteen} 
                        removeBeneficiary={this.props.removeBeneficiary}
                        allState={this.props.allState} removeBeneficiary={this.props.removeBeneficiary} 
                        singleBeneficiary={singleBeneficiary}/>
                )) }

                <div className="justify-content-center tab-profile3-hr"><hr/></div>

                <div className="form-row justify-content-center">
                    <div className="col-8">
                        <h3>CHARITIES</h3>
                        {this.showOrNotShowCharityMessage()}
                    </div>
                    <div className="col-4 tab-plus-icon">
                        <button className="button_1 floatright display_none" onClick={() => props.changeDistributionSection(2)}>Add Beneficiary <span><FontAwesomeIcon icon="plus"/></span></button>
                    </div>
                </div>

                {this.props.allState.distribution.charities.map((singleCharity,index)=>(
                    <SingleCharity 
                        key={index} 
                        charity_number={index} 
                        icon_fifteen={this.props.icon_fifteen} 
                        allState={this.props.allState} 
                        removeCharity={this.props.removeCharity} 
                        singleCharity={singleCharity}/>
                )) }
                <div id="distribute-div" className="form-row profile-checkboxdiv">
                    <div className="col-7">
                        <input type="checkbox" className={"form-check-input "+(props.allState.distribution_one_errors && props.allState.distribution_one_errors.distribution_one_checkbox_error==true ? 'error_outline' : '')} id="distribution_one_checkbox" name="distribution_one_checkbox" onClick={(e)=> this.props.setDistributionOneCheckbox(e.target)}/>
                        <label id="profilelabel2" className="form-check-label" htmlFor="exampleCheck1">I accept that any beneficiary of this may disclaim any interest in my Estate in whole or in part.</label>
                    </div>
                    <div className="col-5">
                        <button id="btn2" className={(props.allState.distribution_one_checkbox == true && props.allState.distribution.beneficiaries.length > 0 ? 'bg_active_button' : '')} onClick={()=>this.props.goToPreviewPage()}>Preview Draft Will</button>
                    </div>
                </div>
            </div>
        );            
    }
}
export default DistributionFirst;