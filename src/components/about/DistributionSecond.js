import React, { Component } from "react";

import CharityPortion from './CharityPortion.js';
import BeneficiaryPortion from './BeneficiaryPortion.js';

class DistributionSecond extends Component {
	constructor(props){
		super(props);
		this.state = {
			rangeValue : 50.00,
			gifted_sum : "",
		}
	}
	
	getActiveType = () => {
		if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'person'){
			return (<p className="bold inline_block"><span className="active_type">BENEFICIARY</span> / <span onClick={()=> this.props.setBeneficiaryType('charity')} className="cursor_pointer">CHARITY</span></p>);	
		}else{
			return (<p className="bold inline_block"><span onClick={()=> this.props.setBeneficiaryType('person')} className="cursor_pointer">BENEFICIARY</span> / <span className="active_type">CHARITY</span></p>);
		}
		
	}
	beneficiaryTypeFields(currentComponentError){
		if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'person'){
			return (<BeneficiaryPortion 
				allState={this.props.allState} 
				checkDistributionValidation={this.props.checkDistributionValidation} 
				setBenificiaryInput={this.props.setBenificiaryInput}
				distributeCheckboxUpdate={this.props.distributeCheckboxUpdate}
				setBenificiaryItemInput={this.props.setBenificiaryItemInput}
				addItemToTemporaryDistribution={this.props.addItemToTemporaryDistribution}
				removeBeneficiaryItem={this.props.removeBeneficiaryItem}/>);
		}else if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'charity'){
			return (<CharityPortion 
				allState={this.props.allState} 
				setCharityInput={this.props.setCharityInput} 
				addCharityObjectToArray={this.props.addCharityObjectToArray}/>)
		}
	}
	beneficiaryHeader(){
		if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'person'){
			return (<h2>Distribution of Estate / Gifts of Money</h2>);
		}else if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'charity'){
			return (<h2>Gifts to Charity</h2>);
		}
	}
	beneficiaryMoto(){
		if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'person'){
			return (<p>You can bequeath your assets (moveable and immoveable) to your beneficiaries here:</p>);
		}else if(this.props.allState.distribution.temporary_beneficiary.beneficiary_type == 'charity'){
			return (<p>You can give an amount of sum to your charity here:</p>);
		}
	}
	render(){
		let props = this.props;
		let classes = (props.active_tab == 4) ? "tab-pane fade show active" : "tab-pane fade" ;
		let currentComponentError = props.allState.distribution_errors;
		return (
			<div className="col-lg-8 offset-2 justify-content-center tab-profile3">
				{this.beneficiaryHeader()}<img className="cursor_pointer" id="close-icon1" src={props.icon_forteen} alt="icon14" onClick={() => props.changeDistributionSection(1)}/>
				{this.beneficiaryMoto()}
				<div className="form-row justify-content-center">
					<div className="col-12">
						<h3> {this.getActiveType()}</h3>
					</div>
					{this.beneficiaryTypeFields(currentComponentError)}
					
				</div>
				
			</div>
		);            
	}
}
export default DistributionSecond;