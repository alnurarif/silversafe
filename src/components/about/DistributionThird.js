import React, { Component } from "react";
class DistributionThird extends Component {
	constructor(props){
		super(props);
	}
	render(){
		let props = this.props;
		let classes = (props.active_tab == 4) ? "tab-pane fade show active" : "tab-pane fade" ;
		return (
			<div className="col-lg-8 offset-2 justify-content-center tab-profile2">
				<h2>Distribution of Estate</h2>
				<p className="paragraph">You can name the beneficiaries of your will here:</p>
				<div className="form-row justify-content-center">
					<div className="col-9">
						<h3>BENEFICIARIES</h3>
					</div>
					<div className="col-3 tab-plus-icon">
						<img src={this.props.icon_thirteen} alt="icon13"/>
					</div>
				</div>
				<div className="form-row distribute-text-wrap justify-content-center">
					<div className="col-4 distribute-text">
						<p>Gan, Jia Jun</p>
						<p>S9403948C</p>
					</div>
					<div className="col-6 distribute-text2">
						<p>Distribution of Estate: <strong>20 %</strong></p>
						<p>Property: <strong>22 Havelock Road S220222</strong></p>
						<p>Gifts of Money: <strong>250 000 </strong>SGD</p>
					</div>
					<div className="col-2 distribute-text-img">
						<img src={this.props.icon_fifteen} alt="icon15"/>
					</div>
				</div>
				<div id="distribute-div" className="form-row profile-checkboxdiv">
					<div className="col-7">
						<input type="checkbox" className="form-check-input" id="exampleCheck2" />
						<label id="profilelabel2" className="form-check-label" htmlFor="exampleCheck1">I accept that any beneficiary of this may disclaim any interest in my Estate in whole or in part.</label>
					</div>
					<div className="col-5">
						<button id="btnnew" type="submit" className="btn btn-primary mb-2">Preview Draft Will</button>
					</div>
				</div>
			</div>
		);            
	}
}
export default DistributionThird;