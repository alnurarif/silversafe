import React, { Component } from "react";
class Item extends Component {
    constructor(props){
        super(props);
    }
    isThereGiftedMesage(){
    	if(this.props.singleBeneficiary.gifted_sum > 0){
    		return (
	    		<p>Gifts of Money: <strong>{this.props.singleBeneficiary.gifted_sum}</strong> SGD</p>
	    	);	
    	}
    	
    }
    showDeleteButton(){
    	if(this.props.allState.distribution.temporary_beneficiary.items.length == (this.props.item_number+1) && this.props.item_number != 0){
    		return (<button className="button_1 floatright" onClick={()=>this.props.removeBeneficiaryItem(this.props.item_number)}>Delete Item <i className="fa fa-minus"></i></button>)
    	}
    }
	render(){
		let beneficiaryItemErrors = this.props.allState.distribution_errors.beneficiaryItemErrors;
		// let currentComponentError = beneficiaryItemErrors[this.props.item_number];
		let currentComponentError;
		if(beneficiaryItemErrors != undefined){
			currentComponentError = beneficiaryItemErrors[this.props.item_number];
		}
		
		return (
			<React.Fragment>
				<div className="col-4 distri-item">
					<label>Item Description</label>
					<input type="text" className={"form-control mb-2"} id="item_description" name="item_description" onChange={(e)=>this.props.setBenificiaryItemInput(e.target,this.props.item_number)}/>
				</div>
				<div className="col-4 distri-item">
					<label>SKU (Reference) Number </label>
					<input type="text" className={"form-control mb-2"} id="sku_number" name="sku_number" onChange={(e)=>this.props.setBenificiaryItemInput(e.target,this.props.item_number)}/>
				</div>
				<div className="col-4 distri-item">
					{this.showDeleteButton()}
				</div>
				<div className="justify-content-center tab-profile3-hr"><hr/></div>
			</React.Fragment>
		);
	}
}
export default Item;