import React, { Component } from "react";
import AppointExecutorsField from "./AppointExecutorsField.js";
import AppointSubstituteExecutorsField from "./AppointSubstituteExecutorsField.js";

class KeyInfoTabOne extends Component {
	constructor(props){
	    super(props);
	    this.state = {
	    	appointment_of_executors_main : this.props.allState.key_info.appointment_of_executors.main,
	    	appointment_of_executors_substitute : this.props.allState.key_info.appointment_of_executors.substitute
	    }
	  }
	  update_tab_state_event(value){
	    this.props.update_tab_state(value);
	  }
	  allExecutors(){
	  	let executors = [];
	    for(let x in this.state.appointment_of_executors_main){
	    	executors.push(<AppointExecutorsField  icon_thirteen={this.props.icon_thirteen} 
          icon_twelve={this.props.icon_twelve} addExecutor={this.props.addExecutor}/>);
	    }
	    return (executors);
	  }
	  allSubstituteExecutors(){
	  	let executors = [];
	    for(let x in this.state.appointment_of_executors_substitute){
	    	executors.push(<AppointExecutorsField  icon_thirteen={this.props.icon_thirteen} 
          icon_twelve={this.props.icon_twelve} addExecutor={this.props.addExecutor}/>);
	    }
	    return (executors);
	  }
	  render(){
	    let props = this.props;
	    let classes = (props.active_tab == 2) ? "tab-pane fade show active" : "tab-pane fade" ;
	    return (
	    	
	    	
	      <div className={classes} id="keyinfo" role="keyinfo" aria-labelledby="keyinfo">
	        <div className="container">
	          <div className="row">
	            <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
	              <div id="div-tab-new" style={{marginLeft: "204px", paddingTop: "30px", display:'table', position: 'relative'}}>
	                <div style={{position : "absolute", width : "100%", height : "32px"}}>
	                  <ul style={{margin: "0px", height: "32px", width: "100%", padding: "0px"}}>
	                    <li style={{float : 'left', width: '32%', marginRight : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(2)}></li>
	                    <li style={{float : 'left', width: '32%', marginRight : '.50%', marginLeft : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(3)}></li>
	                    <li style={{float : 'left', width: '32%', marginRight : '0%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(4)}></li>
	                  </ul>
	                </div>
	                <img style={{marginLeft: "0px", paddingTop: "0px"}} className="tab-icon1" src={props.tab_icon_one} alt="tab-icon1"/>
	              </div>
	              <h2>Appointment of Executors</h2>
	              <p>Executors are the ones executing your will when the testator passes on. There can be additional joint executors and substitute executors added into your will.</p>
	              
					{props.allState.key_info.appointment_of_executors.main.map((counter,index) => ( 
						<AppointExecutorsField 
							key={index}
							icon_thirteen={this.props.icon_thirteen} 
							icon_thirteen_minus={this.props.icon_thirteen_minus} 
          					icon_twelve={this.props.icon_twelve} 
          					addExecutor={this.props.addExecutor} 
          					removeExecutor={this.props.removeExecutor}
          					allState={props.allState}
          					executor_number={index+1}
          					setExecutorChangedValue={props.setExecutorChangedValue}/>
						))
					}

					{props.allState.key_info.appointment_of_executors.substitute.map((counter,index) => ( 
						<AppointSubstituteExecutorsField 
							key={index}
							icon_thirteen={this.props.icon_thirteen} 
							icon_thirteen_minus={this.props.icon_thirteen_minus} 
          					icon_twelve={this.props.icon_twelve} 
          					addSubstituteExecutor={this.props.addSubstituteExecutor} 
          					removeSubstituteExecutor={this.props.removeSubstituteExecutor}
          					allState={props.allState}
          					executor_number={index+1}
          					setSubstituteExecutorChangedValue={props.setSubstituteExecutorChangedValue}/>
						))
					}

	              <div className="form-row profile-checkboxdiv">
	                <div className="col-7">
	                  <input type="checkbox" className={"form-check-input "+(props.allState.key_info_one_errors.keyInfoOneErrors && props.allState.key_info_one_errors.keyInfoOneErrors.key_info_one_checkbox_error==true ? 'error_outline' : '')} id="key_info_one_checkbox" name="key_info_one_checkbox" onClick={(e)=> this.props.setKeyInfoOneCheckbox(e.target)}/>
	                  <label id="profilelabel2" className="form-check-label" htmlFor="exampleCheck1">I understand that if my main executor dies, my secondary and<br/> substitute executors will represent my interests instead.</label>
	                </div>
	                <div className="col-5">
	                  <button id="btn1" type="submit" className="btn btn-primary mb-2" onClick={()=>this.props.validateAndGo(3)}>Next</button>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    );            
	  }
}
export default KeyInfoTabOne;