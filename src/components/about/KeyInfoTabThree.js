import React, { Component } from "react";
class KeyInfoTabThree extends Component {
	constructor(props){
            super();
          }
          update_tab_state_event(value){
            this.props.update_tab_state(value);
          }
          render(){
            let props = this.props;
            let classes = (props.active_tab == 4) ? "tab-pane fade show active" : "tab-pane fade" ;
            return (
              <div className={classes} id="keyinfo" role="keyinfo" aria-labelledby="keyinfo">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
                      <div id="div-tab-new" style={{marginLeft: "204px", paddingTop: "30px", display:'table', position: 'relative'}}>
                        <div style={{position : "absolute", width : "100%", height : "32px"}}>
                          <ul style={{margin: "0px", height: "32px", width: "100%", padding: "0px"}}>
                            <li style={{float : 'left', width: '32%', marginRight : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(2)}></li>
                            <li style={{float : 'left', width: '32%', marginRight : '.50%', marginLeft : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(3)}></li>
                            <li style={{float : 'left', width: '32%', marginRight : '0%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(4)}></li>
                          </ul>
                        </div>
                        <img style={{marginLeft: "0px", paddingTop: "0px"}} className="tab-icon1" src={props.tab_icon_three} alt="tab-icon3"/>
                      </div>

                      <h2>Administration of Estate</h2>
                      <p>I am hereby yielding authority to my Executors to:</p>
                      <p>(a)  sell, call in and convert my Estate into money with power to postpone the sale, calling in and conversion thereof for so long as my Executor may in his/her absolute discretion think fit without being liable for loss; and
                      </p>
                      <p>(b)  distribute my Estate in its original form or distribute the net proceeds of such sale, calling in and conversion and any ready monies belonging to me at my demise in accordance with the clauses of this Will.
                      </p>
                      <p>(c)  My Executor may out of the ready monies forming part of my Estate pay my debts (if any), funeral and testamentary expenses (if any), estate duties (if any) and other taxes payable by reason of my death and shall stand possessed of the rest of my Estate to be dealt with in accordance with the following clauses of this Will.</p>
                      <div id="key3-div" className="form-row profile-checkboxdiv">
                        <div className="col-6">
                          <input type="checkbox" className={"form-check-input "+(props.allState.key_info_three_errors && props.allState.key_info_three_errors.key_info_three_checkbox_error==true ? 'error_outline' : '')} id="key_info_three_checkbox" name="key_info_three_checkbox" onClick={(e)=> this.props.setKeyInfoThreeCheckbox(e.target)}/>
                          <label id="profilelabel2" className="form-check-label" htmlFor="exampleCheck1">I understand that this form is legally binding and the terms and conditions upon which the contract is contingent upon.</label>
                        </div>
                        <div className="col-6">
                          <button id="btn1" type="submit" className="btn btn-primary mb-2" onClick={()=>this.props.validateAndGo(5)}>Agree and Proceed</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );            
          }
}
export default KeyInfoTabThree;