import React, { Component } from "react";
import AppointGuardiansField from "./AppointGuardiansField.js";
import AppointSubstituteGuardiansField from "./AppointSubstituteGuardiansField.js";
class KeyInfoTabTwo extends Component {
    constructor(props){
        super(props);
        this.state = {
            appointment_of_guardians : this.props.allState.key_info.appointment_of_guardians.main,
            appointment_of_guardians_substitute : this.props.allState.key_info.appointment_of_guardians.substitute
        }
    }
    update_tab_state_event(value){
        this.props.update_tab_state(value);
    }
    allExecutors(){
        let guardians = [];
        for(let x in this.state.appointment_of_guardians){
            guardians.push(<AppointGuardiansField  icon_thirteen={this.props.icon_thirteen} 
            icon_twelve={this.props.icon_twelve} addGuardian={this.props.addGuardian}/>);
        }
        return (guardians);
    }
    render(){
        let props = this.props;
        let classes = (props.active_tab == 3) ? "tab-pane fade show active" : "tab-pane fade" ;
        return (
            <div className={classes} id="keyinfo" role="keyinfo" aria-labelledby="keyinfo">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
                            <div id="div-tab-new" style={{marginLeft: "204px", paddingTop: "30px", display:'table', position: 'relative'}}>
                                <div style={{position : "absolute", width : "100%", height : "32px"}}>
                                    <ul style={{margin: "0px", height: "32px", width: "100%", padding: "0px"}}>
                                        <li style={{float : 'left', width: '32%', marginRight : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(2)}></li>
                                        <li style={{float : 'left', width: '32%', marginRight : '.50%', marginLeft : '.50%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(3)}></li>
                                        <li style={{float : 'left', width: '32%', marginRight : '0%', listStyle : 'none', height: '100%', cursor: 'pointer', lineHeight: '32px'}} onClick={() => this.props.validateAndGo(4)}></li>
                                    </ul>
                                </div>
                                <img style={{marginLeft: "0px", paddingTop: "0px"}} className="tab-icon1" src={props.tab_icon_two} alt="tab-icon2"/>
                            </div>
                            <h2>Appointment of Guardians</h2>
                            <p>Guardians are the people who will take care of your children who are under the age of 21 at the time of your death.</p>
                            {props.allState.key_info.appointment_of_guardians.main.map((counter,index) => ( 
                                <AppointGuardiansField 
                                    key={index}
                                    icon_thirteen={this.props.icon_thirteen} 
                                    icon_thirteen_minus={this.props.icon_thirteen_minus} 
                                    icon_twelve={this.props.icon_twelve} 
                                    addGuardian={this.props.addGuardian} 
                                    removeGuardian={this.props.removeGuardian}
                                    allState={props.allState}
                                    guardian_number={index+1}
                                    setGuardianChangedValue={props.setGuardianChangedValue}/>
                                ))
                            }
                            {props.allState.key_info.appointment_of_guardians.substitute.map((counter,index) => ( 
                                <AppointSubstituteGuardiansField 
                                    key={index}
                                    icon_thirteen={this.props.icon_thirteen} 
                                    icon_thirteen_minus={this.props.icon_thirteen_minus} 
                                    icon_twelve={this.props.icon_twelve} 
                                    adGuardian={this.props.adGuardian} 
                                    removeGuardian={this.props.removeGuardian}
                                    allState={props.allState}
                                    guardian_number={index+1}
                                    setGuardianChangedValue={props.setGuardianChangedValue}
                                    setSubstituteGuardianChangedValue={props.setSubstituteGuardianChangedValue}/>
                                ))
                            }
                            <div className="form-row profile-checkboxdiv">
                                <div className="col-7">
                                    <input type="checkbox" className={"form-check-input "+(props.allState.key_info_two_errors.keyInfoTwoErrors && props.allState.key_info_two_errors.keyInfoTwoErrors.key_info_two_checkbox_error==true ? 'error_outline' : '')} id="key_info_two_checkbox" name="key_info_two_checkbox" onClick={(e)=> this.props.setKeyInfoTwoCheckbox(e.target)}/>
                                    <label id="profilelabel2" className="form-check-label" htmlFor="exampleCheck1">I understand that if my main guardian dies, my secondary and substitute guardians will represent my children’s interests instead.</label>
                                </div>
                                <div className="col-5">
                                    <button id="btn1" type="submit" className="btn btn-primary mb-2" onClick={()=>this.props.validateAndGo(4)}>Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );            
    }
}
export default KeyInfoTabTwo;