import React, { Component } from "react";
class NextPreviousSection extends Component {
  constructor(props){
    
    super(props);
    this.state = {
      active_tab : this.props.active_tab
    };
    this.increamentTab = this.increamentTab.bind(this);
    this.decreamentTab = this.decreamentTab.bind(this);

  }
  update_tab_state_event(value){
    this.props.validateAndGo(value);
  }
  increamentTab(){

    if(this.props.active_tab == 6){
      this.setState({
        active_tab : 1
      })
      this.update_tab_state_event(1)
    }else{
      this.setState({
        active_tab : this.props.active_tab + 1
      })
      this.update_tab_state_event(this.props.active_tab+1)
    }
  }
  decreamentTab(){

    if(this.props.active_tab == 1){
      this.setState({
        active_tab : 6
      })
      this.update_tab_state_event(6)
    }else{
      this.setState({
        active_tab : this.props.active_tab - 1
      })
      this.update_tab_state_event(this.props.active_tab -1 )
    }
  }
  render(){
    let props = this.props;
    return (
      <div>
        <div className="divclass-button" style={{position : "absolute"}}>
          <img className="prevtab" src={props.left_arrow} alt="tab-left"  onClick={this.decreamentTab}/>
          <img className="nexttab" src={props.right_arrow} alt="tab-right"  onClick={this.increamentTab}/>
        </div>
        
      </div>
      
    );            
  }
}
export default NextPreviousSection;