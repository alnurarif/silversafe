import React, { Component } from "react";
import jsPDF from "jspdf";
import 'jspdf-autotable'; 
import moment from "moment";

class Preview extends Component {
	constructor(props){
    super(props);
  }
    ordinal(number) {
        switch (number) {
            case 1:
            case 21:
            return number + 'st';
            break;
            case 2:
            case 22:
            return number + 'nd';
            break;
            case 3:
            case 23:
            return number + 'rd';
            break;
            default:
            return number + 'th';
        }
    }
  showPdf(){
    var allData = this.props.allState;
    var doc = new jsPDF();
    let smallAlphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

    var lMargin=15; //left margin in mm
    var rMargin=15; //right margin in mm
    var pdfInMM=210;
    

    let str = "DATED OF "+moment().format("DD MMMM yyyy");
    doc.setTextColor(0,0,0);
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 40, 'center');

    str = "===========================================================";
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 140, 'center');


    str = "LAST WILL AND TESTAMENT";
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 160, 'center');

    str = "OF";
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 175, 'center');

    str = allData.profile_first_name+' '+allData.profile_last_name;
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 190, 'center');

    str = "===========================================================";
    doc.setFontSize(14);
    doc.text(str, pageWidth / 2, 210, 'center');


    doc.addPage();
    doc.setFontSize(12);
    str = 'I, ';
    doc.text(str, 15, 20);
    str = allData.profile_first_name+' '+allData.profile_last_name;
    doc.text(str, 70, 20);
    doc.line(18,21,180,21);
    str = 'NRIC';
    doc.text(str, 180, 20);

    doc.line(23,27,96,27);
    str = 'No.:                    '+allData.profile_nric;
    doc.text(str, 15, 26);

    doc.line(110,27,185,27);
    str = "; DOB:                  "+moment(allData.profile_date_of_birth_unformatted).format('DD MMMM');
    doc.text(str, 98, 26);

    str = " of";
    doc.text(str, 186, 26);


    doc.line(15,33,108,33);
    str = moment(allData.profile_date_of_birth_unformatted).format('yyyy');
    doc.text(str, 50, 32);

    str = ', do hereby REVOKE all my former wills and';
    doc.text(str, 108, 32);


    str = 'testamentary dispositions in respect of my Estate and declare this to be my last Will and ';
    doc.text(str, 15, 39);

    str = 'Testament in respect of my Estate.';
    doc.text(str, 15, 46);
    

    str = '1.          In this Will:';
    doc.text(str, 15, 60);    


    str='(i) the expression "my Estate" shall mean all my real and personal property of whatsoever nature and wheresoever situated, including but not limited to any bank accounts; and';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,70,lines);

    str='(ii) the expression "my Executor" shall mean the executor and trustee for the time being of this Will whether original or substituted.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,90,lines);

    str = '2.          APPOINTMENT OF EXECUTORS AND TRUSTEES';
    doc.text(str, 15, 110);    

    let allExecutors = "";
    let i = 1;
    let executorOrExecutors = (allData.key_info.appointment_of_executors.main.length > 1 ) ? 'executors and trustees' : 'only executor and trustee';
    allData.key_info.appointment_of_executors.main.forEach((executorMain) => {
        if(i == 1){
            allExecutors += executorMain.first_name +' '+executorMain.last_name + ' '+executorMain.nric;
        }else if(i == allData.key_info.appointment_of_executors.main.length){
            allExecutors += ', and '+executorMain.first_name +' '+executorMain.last_name + ' '+executorMain.nric;
        }else{
            allExecutors += ', and '+executorMain.first_name +' '+executorMain.last_name + ' '+executorMain.nric;
        }
        i++;
    });

    let allSubstituteExecutors = "";
    i = 1;
    allData.key_info.appointment_of_executors.substitute.forEach((executorSubstitute) => {
        if(i == 1){
            allSubstituteExecutors += executorSubstitute.first_name +' '+executorSubstitute.last_name + ' '+executorSubstitute.nric;
        }else if(i == allData.key_info.appointment_of_executors.main.length){
            allSubstituteExecutors += ', and '+executorSubstitute.first_name +' '+executorSubstitute.last_name + ' '+executorSubstitute.nric;
        }else{
            allSubstituteExecutors += ', and '+executorSubstitute.first_name +' '+executorSubstitute.last_name + ' '+executorSubstitute.nric;
        }
        i++;
    });
    let substituteHeShe = '';
    let substituteHeShe2 = '';
    if(allData.key_info.appointment_of_executors.main.length == 1 && allData.key_info.appointment_of_executors.main[0].gender == 'Male'){
        substituteHeShe = "he";
        substituteHeShe2 = "his";
    }else if(allData.key_info.appointment_of_executors.main.length == 1 && allData.key_info.appointment_of_executors.main[0].gender == 'Female'){
        substituteHeShe = "she";
        substituteHeShe2 = "her";
    }else if(allData.key_info.appointment_of_executors.main.length == 2){
        substituteHeShe = "either of them";
        substituteHeShe2 = "their";
    }else if(allData.key_info.appointment_of_executors.main.length > 2){
        substituteHeShe = "any of them";
        substituteHeShe2 = "their";
    }

    str='I appoint '+allExecutors+' to be the '+executorOrExecutors+' of this will.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,120,lines);

    str='If '+substituteHeShe+' dies before me, refuses to act or is unable to act, or '+substituteHeShe2+' appointment does not take effect for any other reason, I appoint '+allSubstituteExecutors+' to fill the resulting vacancy or vacancies.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,140,lines);


    let allGuardians = "";
    let guardianGender = ""
    i = 1;
    allData.key_info.appointment_of_guardians.main.forEach((guardianMain) => {
        if(i == 1){
            allGuardians += guardianMain.first_name +' '+guardianMain.last_name + ' '+guardianMain.nric;
            guardianGender = (guardianMain.gender == "Male") ? 'his' : 'her';
        }else if(i == allData.key_info.appointment_of_executors.main.length){
            allGuardians += ', and '+guardianMain.first_name +' '+guardianMain.last_name + ' '+guardianMain.nric;
        }else{
            allGuardians += ', and '+guardianMain.first_name +' '+guardianMain.last_name + ' '+guardianMain.nric;
        }
        i++;
    });

    let allSubstituteGuardians = "";
    i = 1;
    allData.key_info.appointment_of_guardians.substitute.forEach((guardianSubstitute) => {
        if(i == 1){
            allSubstituteGuardians += guardianSubstitute.first_name +' '+guardianSubstitute.last_name + ' '+guardianSubstitute.nric;
        }else if(i == allData.key_info.appointment_of_executors.main.length){
            allSubstituteGuardians += ', and '+guardianSubstitute.first_name +' '+guardianSubstitute.last_name + ' '+guardianSubstitute.nric;
        }else{
            allSubstituteGuardians += ', and '+guardianSubstitute.first_name +' '+guardianSubstitute.last_name + ' '+guardianSubstitute.nric;
        }
        i++;
    });

    let husbandWifeMessage = '';

    if(allData.profile_gender == 'Male' && (allData.profile_marital_status == 'Married' || allData.profile_marital_status == 'Separated')){
        husbandWifeMessage = 'If my wife dies before me, ';        
    }else if(allData.profile_gender == 'Female' && (allData.profile_marital_status == 'Married' || allData.profile_marital_status == 'Separated')){
        husbandWifeMessage = 'If my husband dies before me, ';        
    }


    str = '3.          APPOINTMENT OF GUARDIANS';
    doc.text(str, 15, 170);    

    str = husbandWifeMessage+'I appoint, as the guardian of any of my children who are under 21 at my death, '+allGuardians+'. ';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,180,lines);

    str='If '+allGuardians+' dies before me, or '+guardianGender+' appointment does not take effect for any other reason, then I appoint '+allSubstituteGuardians+' instead.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,200,lines);



    str = '4.          ADMINISTRATION OF ESTATE';
    doc.text(str, 15, 230);    

    str='(i) My Executor shall have the power to:';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,240,lines);

    str='(a) sell, call in and convert my Estate into money with power to postpone the sale, calling in and conversion thereof for so long as my Executor may in his/her absolute discretion think fit without being liable for loss; and';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-30-rMargin));
    doc.text(lMargin+30,250,lines);

    doc.addPage();

    str='(b) distribute my Estate in its original form or distribute the net proceeds of such sale, calling in and conversion and any ready monies belonging to me at my demise in accordance with the clauses of this Will.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-30-rMargin));
    doc.text(lMargin+30,20,lines);

    str='(ii) My Executor may out of the ready monies forming part of my Estate pay my debts (if any), funeral and testamentary expenses (if any), estate duties (if any) and other taxes payable by reason of my death and shall stand possessed of the rest of my Estate to be dealt with in accordance with the following clauses of this Will.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,40,lines);

    str = '5.          DISTRIBUTION OF ESTATE';
    doc.text(str, 15, 70); 

    str='(i) Subject to the other clauses in this Will, I GIVE, my moveable and immoveable assets of whatever nature, in any part of the world, not specifically or fully devised and bequeathed by earlier provisions to the following beneficiaries:';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,80,lines);

    let allBeneficiaries = "";
    i = 1;
    let top = 100;
    allData.distribution.beneficiaries.forEach((beneficiary,index) => {
        
        str=smallAlphabet[index]+'. '+beneficiary.allocation_of_state_range+'% of my ESTATE to '+beneficiary.first_name+' '+beneficiary.last_name+' '+beneficiary.nric+';';
        var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-30-rMargin));
        doc.text(lMargin+30,top,lines);
        top = top+10;
    });

    str='(ii) If any one of the abovenamed beneficiaries is not living at my death, I give his/her interest in my assets to the remaining surviving beneficiaries in equal shares.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,top,lines);
    

    let currentPoint = 5;


     

    

    let allBeneficiariesProperty = "";
    i = 1;
    top = top + 20;
    allData.distribution.beneficiaries.forEach((beneficiary) => {
        if(beneficiary.address != ""){
            if(i == 1){
                currentPoint += 1;

                str = currentPoint+'.          REAL PROPERTY';
                doc.text(str, 15, top);        
                top = top + 10;
                i++;    
            }

            str='Property:  all my interest in the property known as '+beneficiary.address+'';
            var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
            doc.text(lMargin+15,top,lines);
            top = top + 10;
            str='Property Donee[s]: '+beneficiary.first_name+' '+beneficiary.last_name+' '+beneficiary.nric;
            var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
            doc.text(lMargin+15,top,lines);
            top = top + 10;    
        }
        
    });

    if(i > 1){
        top = top + 10;
        str='(i) I give the Property absolutely to the Property Donee';
        var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
        doc.text(lMargin+15,top,lines);

        top = top + 10;
        str='(ii) My Trutopes must pay from my Estate any mortgage debt or other sums secured on the Property (to the extent that they are not paid from the proceeds of a life assurance policy given as additional security for payment), and any related interest or expenses.';
        var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
        doc.text(lMargin+15,top,lines);    
    }
    

    
    doc.addPage();


    top = 20;


    let gift_text = "";

    i = 0;
    allData.distribution.beneficiaries.forEach((beneficiary,index) => {
        if(beneficiary.gifted_sum > 0){
            
            i++;
        }
    });
    gift_text = (i > 1) ? 'GIFTS' : 'GIFT';

    i = 1;
    allData.distribution.beneficiaries.forEach((beneficiary,index) => {
        if(beneficiary.gifted_sum > 0){
            if(i == 1){
                currentPoint += 1;                
                str = currentPoint+'.';
                doc.text(str, 15, top);

                str = gift_text+' OF MONEY';
                doc.text(str, lMargin+15, top);    
                
                top = top + 10;

                str='(i) I give:';
                var lines = doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
                doc.text(lMargin+15,top,lines);
            }
            top = top + 10;
            str=smallAlphabet[index]+'. S$'+beneficiary.gifted_sum+' to '+beneficiary.first_name+' '+beneficiary.last_name+' '+beneficiary.nric+';';
            var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-30-rMargin));
            doc.text(lMargin+30,top,lines);
            
            i++;            
        }
    });

    top = top + 10;

    gift_text = "";

    i = 0;
    allData.distribution.charities.forEach((charity,index) => {
        if(charity.charity_gifted_sum > 0){
            
            i++;
        }
    });
    gift_text = (i > 1) ? 'GIFTS' : 'GIFT';

    i = 1;
    allData.distribution.charities.forEach((charity,index) => {
        if(charity.charity_gifted_sum > 0){
            if(i == 1){
                currentPoint += 1;                
                str = currentPoint+'.';
                doc.text(str, 15, top);

                str = gift_text+' TO CHARITY';
                doc.text(str, lMargin+15, top);
                               
                
                top = top + 10;

                str='(ii) I give:';
                var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
                doc.text(lMargin+15,top,lines);
            }
            top = top + 10;
            str=smallAlphabet[index]+'. S$'+charity.charity_gifted_sum+' to '+charity.charity_name+';';
            var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-30-rMargin));
            doc.text(lMargin+30,top,lines);
            i++;            
        }
    });

      
    
    if(i > 1){

        top = top + 10;

        str='(iii) If, at my death, any of the charities named above ("Original Charity") no longer exists or is being wound up, my Trustees must pay the amount specified above as being payable to the Original Charity to another charity that has objects similar to the Original Charity.';
        var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
        doc.text(lMargin+15,top,lines);
        
        top = top + 25;
    }    


    currentPoint++;
    
    
    str = currentPoint+'.';
    doc.text(str, 15, top);

    str = 'POWER FOR BENEFICIARIES TO DISCLAIM GIFTS';
    doc.text(str, lMargin+15, top);
    
    top = top + 10;
    str='Any beneficiary of this will may disclaim any interest in my Estate in whole or in part.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,top,lines);

    currentPoint++;
    top = top + 10;
    
    str = currentPoint+'.';
    doc.text(str, 15, top);

    str = 'THE SCHEDULE AND ADMINISTRATIVE PROVISIONS';
    doc.text(str, lMargin+15, top);


    top = top + 10;
    str='The Schedules form part of this will and has effect as if set out in full in the body of this will and any reference to this will includes the Schedules.';
    var lines =doc.splitTextToSize(str, (pdfInMM-lMargin-15-rMargin));
    doc.text(lMargin+15,top,lines);

    doc.addPage();


    str = 'IN WITNESS WHEREOF I have here unto set my hand this '+this.ordinal(parseInt(moment().format('DD').substr(moment().format('DD').length - 1)))+' day of '+moment().format('MMMM')+', '+moment().format('yyyy')+'.';
    doc.text(str, 15, 20);

    str = 'SIGNED by';
    doc.text(str, 15, 60);

    str = 'the Testator as and for his last Will and Testament of';
    doc.text(str, 15, 70);

    str = 'his Estate in the presence of us';
    doc.text(str, 15, 80);

    str = 'both present at the same time who in his';
    doc.text(str, 15, 90);

    str = 'presence at his request and in the presence';
    doc.text(str, 15, 100);

    str = 'of each other have hereto subscribed our';
    doc.text(str, 15, 110);

    str = 'names as witnesses attesting the same on the';
    doc.text(str, 15, 120);

    str = 'day and year above written.';
    doc.text(str, 15, 130);

    str = 'WITNESSES:';
    doc.text(str, 15, 170);


    str = '______________________________';
    doc.text(str, 15, 190);

    str = '______________________________';
    doc.text(str, 110, 190);

    str = 'Name:';
    doc.text(str, 15, 200);

    str = 'Name:';
    doc.text(str, 110, 200);

    str = 'NRIC / Passport No:';
    doc.text(str, 15, 210);

    str = 'NRIC / Passport No:';
    doc.text(str, 110, 210);




//     doc.addPage();

//     var col = ["S/N","Asset Type","Description of Asset"];
//        var rows = [];

//   /* The following array of object as response from the API req  */



// var itemNew = [

//   { index:'1',asset_type: 'Case Number', asset_description : '101111111' },
//   { index:'2',asset_type: 'Patient Name', asset_description : 'UAT DR' },
//   { index:'3',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'4',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'5',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'6',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'7',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'8',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'9',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' },
//   { index:'10',asset_type: 'Hospital Name', asset_description: 'Dr Abcd' }

// ]


//    itemNew.forEach(element => {      
//         var temp = [element.index,element.asset_type,element.asset_description];
//         rows.push(temp);

//     });        

//         doc.autoTable(col, rows, { startY: 20 });




    // doc.text(str, pageWidth / 2, pageHeight  - 10, 'center');
    // doc.save("legacy_will_form.pdf");
    window.open(doc.output('bloburl'), '_blank');
  }
  
          render(){
            let props = this.props;
            let classes = (props.active_tab == 6) ? "tab-pane fade show active" : "tab-pane fade" ;
            return (
              <div className={classes} id="preview" role="preview" aria-labelledby="preview">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
                      <div className="col-5 pdf-download">
                          <button onClick={()=>this.showPdf()}>Download pdf</button>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
            );

          }
}
export default Preview;