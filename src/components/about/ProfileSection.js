import React, { Component } from "react";
import ProfileHeader from "./ProfileHeader.js"; 
import TabPills from "./TabPills.js"; 
import TabContents from "./TabContents.js";
import moment from "moment";
class ProfileSection extends Component {
	constructor(props){
		super(props);
		this.state = {
			active_tab : 1,
			active_distribute_section : 1,
			profile_first_name : "",
			profile_last_name : "",
			profile_date_of_birth_unformatted : new Date('01-01-1980'),
			profile_date_of_birth : moment().format("DD MMMM yyyy"),
			profile_nric : "",
			profile_address : "",
			profile_country_of_birth : "",
			profile_country_of_residence : "",
			profile_gender : "",
			profile_marital_status : "",
			profile_checkbox : false,
			key_info_one_checkbox : false,
			key_info_two_checkbox : false,
			key_info_three_checkbox : false,
			beneficiary_checkbox_one : false,
			beneficiary_checkbox_two : false,
			beneficiary_checkbox_three : false,
			distribution_one_checkbox : false,
			profile_errors : {

			},
			charity_errors : {

			},
			key_info_one_errors : {
				keyInfoOneErrors : {
					main : {

					},
					substitute : {

					}
				}
			},
			key_info_two_errors : {
				keyInfoTwoErrors : {
					main : {

					},
					substitute : {

					}
				}
			},
			key_info_three_errors : {

			},
			distribution_one_errors : {

			},
			distribution_errors : {

			},
			key_info : {
				appointment_of_executors : {
					main : [
						{
							first_name : "",
							last_name : "",
							nric : "",
							country_of_birth : "",
							country_of_residence : "",
							gender : ""
						}
					],
					substitute : [
						{
							first_name : "",
							last_name : "",
							nric : "",
							country_of_birth : "",
							country_of_residence : "",
							gender : ""
						}
					]
				},
				appointment_of_guardians : {
					main : [
						{
							first_name : "",
							last_name : "",
							nric : "",
							country_of_birth : "",
							country_of_residence : "",
							gender : ""
						}
					],
					substitute : [
						{
							first_name : "",
							last_name : "",
							nric : "",
							country_of_birth : "",
							country_of_residence : "",
							gender : ""
						}
					]
				}
			},
			distribution : {
				temporary_beneficiary : {
					beneficiary_type : "person",
					first_name : "",
					last_name : "",
					nric : "",
					charity_name : "",
					charity_address : "",
					allocation_of_state_range : 50.00,
					address : "",
					gifted_sum : "",
					items : [
						{
							item_description : "",
							sku_number : ""
						}
					]
				},
				temporary_charity : {
					charity_name : "",
					charity_address : "",
					charity_gifted_sum : "",
				},
				beneficiaries : [],
				charities : []
			}
		}
	}

	/** Profile Section **/
	handleDateOfBirthChange = (date) => {
		this.setState({
	      profile_date_of_birth_unformatted: date,
	      profile_date_of_birth : moment(date).format("DD MMMM yyyy")

	    });
	    
	}
	checkProfileValidation = () => {
		let object = {}
		object.profile_first_name = (this.state.profile_first_name == "") ? true : false;
		object.profile_last_name = (this.state.profile_last_name == "") ? true : false;
		object.profile_date_of_birth = (this.state.profile_date_of_birth == "") ? true : false;
		object.profile_nric = (this.state.profile_nric == "") ? true : false;
		object.profile_address = (this.state.profile_address == "") ? true : false;
		object.profile_country_of_birth = (this.state.profile_country_of_birth == "") ? true : false;
		object.profile_country_of_residence = (this.state.profile_country_of_residence == "") ? true : false;
		object.profile_gender = (this.state.profile_gender == "") ? true : false;
		object.profile_marital_status = (this.state.profile_marital_status == "") ? true : false;
		object.profile_checkbox = (this.state.profile_checkbox == false) ? true : false;
		
		let errors_number = 0;
		for(var o in object){
			errors_number = (object[o] == true) ? errors_number + 1 : errors_number;
		}
		this.setState({
			profile_errors : object
		});
		return errors_number;
	}
	setChangedValue = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){
			value = (this.state[name] == true) ? false : true;
		}
		this.setState({
			[name] : value
		})
	}
	/** Profile Section End **/	
	/**======================================**/
	/**Key Info 1 Executor Section**/
	addExecutor = () => {
		let current_executors = this.state.key_info.appointment_of_executors.main;
		current_executors.push(
			{
				first_name : "",
				last_name : "",
				nric : "",
				country_of_birth : "",
				country_of_residence : "",
				gender : ""
			}
		);
		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_executors: {
					...this.state.key_info.appointment_of_executors,
					main : current_executors
				} 
				
			}
		});
	}
	addSubstituteExecutor = () => {
		let current_executors = this.state.key_info.appointment_of_executors.substitute;
		current_executors.push(
			{
				first_name : "",
				last_name : "",
				nric : "",
				country_of_birth : "",
				country_of_residence : "",
				gender : ""
			}
		);
		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_executors: {
					...this.state.key_info.appointment_of_executors,
					substitute : current_executors
				} 
				
			}
		});
	}
	removeExecutor = () => {
		let current_executors = this.state.key_info.appointment_of_executors.main;
		current_executors = current_executors.slice(0, current_executors.length - 1);

		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_executors: {
					...this.state.key_info.appointment_of_executors,
					main : current_executors
				}
			}
		});	
	}
	removeSubstituteExecutor = () => {
		let current_executors = this.state.key_info.appointment_of_executors.substitute;
		current_executors = current_executors.slice(0, current_executors.length - 1);

		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_executors: {
					...this.state.key_info.appointment_of_executors,
					substitute : current_executors
				}
			}
		});	
	}
	checkKeyInfoOneValidation = () => {
		let main = [];
		let substitute = [];
		
		this.state.key_info.appointment_of_executors.main.map((singleExecutor,index) => {
			let object = {};
			object.first_name = (singleExecutor.first_name == "") ? true : false;
			object.last_name = (singleExecutor.last_name == "") ? true : false;
			object.nric = (singleExecutor.nric == "") ? true : false;
			object.country_of_birth = (singleExecutor.country_of_birth == "") ? true : false;
			object.country_of_residence = (singleExecutor.country_of_residence == "") ? true : false;
			object.gender = (singleExecutor.gender == "") ? true : false;
			main.push(object);
		});
		this.state.key_info.appointment_of_executors.substitute.map((singleExecutor,index) => {
			let object = {};
			object.first_name = (singleExecutor.first_name == "") ? true : false;
			object.last_name = (singleExecutor.last_name == "") ? true : false;
			object.nric = (singleExecutor.nric == "") ? true : false;
			object.country_of_birth = (singleExecutor.country_of_birth == "") ? true : false;
			object.country_of_residence = (singleExecutor.country_of_residence == "") ? true : false;
			object.gender = (singleExecutor.gender == "") ? true : false;
			substitute.push(object);
		});
		let key_info_one_checkbox_error = (this.state.key_info_one_checkbox == false) ? true : false;
		
		this.setState({
			key_info_one_errors: {
				...this.state.key_info_one_errors,
				keyInfoOneErrors: {
					...this.state.key_info_one_errors.keyInfoOneErrors,
					main,
					substitute,
					key_info_one_checkbox_error

				}
			}
		});	
		
		let errors_number = 0;
		main.map((singleError,index) => {
			errors_number = (singleError.first_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.last_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.nric == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_birth == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_residence == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.gender == true) ? errors_number + 1 : errors_number;
		});
		substitute.map((singleError,index) => {
			errors_number = (singleError.first_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.last_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.nric == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_birth == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_residence == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.gender == true) ? errors_number + 1 : errors_number;
		});
		errors_number = (this.state.key_info_one_checkbox == false) ? errors_number + 1 : errors_number;
		
		return errors_number;
	}
	setKeyInfoOneCheckbox = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){

			value = (this.state.key_info_one_checkbox == true) ? false : true;
		}
		this.setState({
			[name] : value
		})	
	}
	setExecutorChangedValue = (target,executorNumber) => {
		let {value,name,type} = target;
		let executorNumberIndex = executorNumber - 1;
		let objectToUpdate = this.state.key_info.appointment_of_executors.main[executorNumberIndex];
		objectToUpdate[name] = value;
	}
	setSubstituteExecutorChangedValue = (target,executorNumber) => {
		let {value,name,type} = target;
		let executorNumberIndex = executorNumber - 1;
		let objectToUpdate = this.state.key_info.appointment_of_executors.substitute[executorNumberIndex];
		objectToUpdate[name] = value;
	}
	/**Key Info 1 Executor Section End**/
	/**======================================**/
	/**Key Info 2 Guardian Section**/
	addGuardian = () => {
		let current_guardians = this.state.key_info.appointment_of_guardians.main;
		current_guardians.push(
			{
				first_name : "",
				last_name : "",
				nric : "",
				country_of_birth : "",
				country_of_residence : "",
				gender : ""
			}
		);
		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_guardians: {
					...this.state.key_info.appointment_of_guardians,
					main : current_guardians	
				}
			}
		});
	}
	removeGuardian = () => {
		let current_guardians = this.state.key_info.appointment_of_guardians.main;
		current_guardians = current_guardians.slice(0, current_guardians.length - 1);

		this.setState({
			key_info: {
				...this.state.key_info,
				appointment_of_guardians: {
					...this.state.key_info.appointment_of_guardians,
					main : current_guardians	
				}
			}
		});	
	}
	checkKeyInfoTwoValidation = () => {
		let keyInfoTwoErrors = [];
		let main = [];
		let substitute = [];
		
		this.state.key_info.appointment_of_guardians.main.map((singleGuardian,index) => {
			let object = {};
			object.first_name = (singleGuardian.first_name == "") ? true : false;
			object.last_name = (singleGuardian.last_name == "") ? true : false;
			object.nric = (singleGuardian.nric == "") ? true : false;
			object.country_of_birth = (singleGuardian.country_of_birth == "") ? true : false;
			object.country_of_residence = (singleGuardian.country_of_residence == "") ? true : false;
			object.gender = (singleGuardian.gender == "") ? true : false;
			main.push(object);
		});
		this.state.key_info.appointment_of_guardians.substitute.map((singleGuardian,index) => {
			let object = {};
			object.first_name = (singleGuardian.first_name == "") ? true : false;
			object.last_name = (singleGuardian.last_name == "") ? true : false;
			object.nric = (singleGuardian.nric == "") ? true : false;
			object.country_of_birth = (singleGuardian.country_of_birth == "") ? true : false;
			object.country_of_residence = (singleGuardian.country_of_residence == "") ? true : false;
			object.gender = (singleGuardian.gender == "") ? true : false;
			substitute.push(object);
		});
		let key_info_two_checkbox_error = (this.state.key_info_two_checkbox == false) ? true : false;
		
		this.setState({
			key_info_two_errors: {
				...this.state.key_info_two_errors,
				keyInfoTwoErrors: {
					...this.state.key_info_two_errors.keyInfoTwoErrors,
					main,
					substitute,
					key_info_two_checkbox_error

				}
			}
		});	

		let errors_number = 0;
		main.map((singleError,index) => {
			errors_number = (singleError.first_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.last_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.nric == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_birth == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_residence == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.gender == true) ? errors_number + 1 : errors_number;
		});
		substitute.map((singleError,index) => {
			errors_number = (singleError.first_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.last_name == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.nric == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_birth == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.country_of_residence == true) ? errors_number + 1 : errors_number;
			errors_number = (singleError.gender == true) ? errors_number + 1 : errors_number;
		});
		errors_number = (this.state.key_info_two_checkbox == false) ? errors_number + 1 : errors_number;
		return errors_number;
	}
	setKeyInfoTwoCheckbox = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){

			value = (this.state.key_info_two_checkbox == true) ? false : true;
		}
		this.setState({
			[name] : value
		})	
	}
	setGuardianChangedValue = (target,guardianNumber) => {
		let {value,name,type} = target;
		let guardianNumberIndex = guardianNumber - 1;
		let objectToUpdate = this.state.key_info.appointment_of_guardians.main[guardianNumberIndex];
		objectToUpdate[name] = value;
	}
	setSubstituteGuardianChangedValue = (target,guardianNumber) => {
		let {value,name,type} = target;
		let guardianNumberIndex = guardianNumber - 1;
		let objectToUpdate = this.state.key_info.appointment_of_guardians.substitute[guardianNumberIndex];
		objectToUpdate[name] = value;
	}
	/**Key Info 2 Guardian Section End**/
	/**======================================**/
	/**Key Info 3 Guardian Section**/
	checkKeyInfoThreeValidation = () => {
		
		let key_info_three_checkbox_error = (this.state.key_info_three_checkbox == false) ? true : false;
		this.setState({
			key_info_three_errors : { key_info_three_checkbox_error}
		});
		let errors_number = 0;
		errors_number = (this.state.key_info_three_checkbox == false) ? errors_number + 1 : errors_number;
		
		return errors_number;
	}
	setKeyInfoThreeCheckbox = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){

			value = (this.state.key_info_three_checkbox == true) ? false : true;
		}
		this.setState({
			[name] : value
		})	
	}
	/**Key Info 3 Guardian Section End**/
	/**======================================**/	
	/**Distribution**/
	goToPreviewPage = () => {
		if(this.state.distribution.beneficiaries.length > 0){
			this.validateAndGo(6);
		}else{
			this.setState({
				active_distribute_section : 2
			});
		}
	}
	setBenificiaryInput = (target) => {
		let {value,name,type} = target;
		this.state.distribution.temporary_beneficiary[name] = value;
	}
	setCharityInput = (target) => {
		let {value,name,type} = target;
		this.state.distribution.temporary_charity[name] = value;
	}
	setBenificiaryItemInput = (target,index) => {
		let {value,name,type} = target;
		let objectToUpdate = this.state.distribution.temporary_beneficiary.items[index];
		objectToUpdate[name] = value;	
	}
	addItemToTemporaryDistribution = () => {
		let updated_items = this.state.distribution.temporary_beneficiary.items;
		updated_items.push(
			{
				item_description : "",
				sku_number : ""
			}
		);
		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        temporary_beneficiary : {
		        	...prevState.distribution.temporary_beneficiary,
		        	items : updated_items
		        }
		    }
		}))
		// this.state.distribution.temporary_beneficiary.items = items;
	}
	removeBeneficiary = (beneficiary_index)=> {
		let withoutCurrent = this.state.distribution.beneficiaries.filter( (c,index) => beneficiary_index !== index);

		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        beneficiaries : withoutCurrent
		    }
		}))
	}
	removeCharity = (charity_index)=> {
		let withoutCurrent = this.state.distribution.charities.filter( (c,index) => charity_index !== index);

		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        charities : withoutCurrent
		    }
		}))
	}
	removeBeneficiaryItem = (item_index) => {
		let withoutCurrent = this.state.distribution.temporary_beneficiary.items.filter( (c,index) => item_index !== index);

		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        temporary_beneficiary : {
		        	...prevState.distribution.temporary_beneficiary,
		        	items : withoutCurrent
		        }
		    }
		}))
	}
	addBeneficiaryObjectToArray = () => {
		let current_beneficiaries = this.state.distribution.beneficiaries;
		current_beneficiaries.push(this.state.distribution.temporary_beneficiary);

		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        beneficiaries : current_beneficiaries
		    }
		}))
		this.state.distribution.temporary_beneficiary = {
			beneficiary_type : "person",
			first_name : "",
			last_name : "",
			nric : "",
			charity_name : "",
			charity_address : "",
			allocation_of_state_range : 50.00,
			address : "",
			gifted_sum : "",
			items : [
				{
					item_description : "",
					sku_number : ""
				}
			]
		};
		this.setState({
		    beneficiary_checkbox_one : false,
		    beneficiary_checkbox_two : false,
		    beneficiary_checkbox_three : false
		})
		this.changeDistributionSection(1);
	}
	addCharityObjectToArray = () => {
		if(this.validateCharity() == 0){
			this.state.distribution.charities.push(this.state.distribution.temporary_charity);
			this.state.distribution.temporary_charity = {
				charity_name : "",
				charity_address : "",
				charity_gifted_sum : "",
			};
			this.changeDistributionSection(1);
			this.setState(prevState => ({
			    ...prevState,
			    distribution: {
			        ...prevState.distribution,
			        temporary_beneficiary : {
			        	...prevState.distribution.temporary_beneficiary,
			        	beneficiary_type : "person"
			        }
			    }
			}))

		}
		
	}
	changeDistributionSection = (value) => {
		this.setState({
			active_distribute_section : value
		});
	}
	setBeneficiaryType = (value) => {
		this.setState(prevState => ({
		    ...prevState,
		    distribution: {
		        ...prevState.distribution,
		        temporary_beneficiary : {
		        	...prevState.distribution.temporary_beneficiary,
		        	beneficiary_type : value
		        }
		    }
		}))
		// this.state.distribution.temporary_beneficiary.beneficiary_type = value;	
	}
	validateCharity = () => {
		let object = {}
		object.charity_name = (this.state.distribution.temporary_charity.charity_name == "") ? true : false;
		object.charity_address = (this.state.distribution.temporary_charity.charity_address == "") ? true : false;
		object.charity_gifted_sum = (this.state.distribution.temporary_charity.charity_gifted_sum == "") ? true : false;
		
		let errors_number = 0;
		for(var o in object){
			errors_number = (object[o] == true) ? errors_number + 1 : errors_number;
		}
		this.setState({
			charity_errors : object
		});
		return errors_number;
	}
	checkDistributionValidation = () => {
		let temp_beneficiary = this.state.distribution.temporary_beneficiary;
		let object = {};
		// let beneficiaryItemErrors = [];
		// this.state.distribution.temporary_beneficiary.items.map((singleItem,index) => {
		// 	let object = {};
		// 	object.item_description = (singleItem.item_description == "") ? true : false;
		// 	object.sku_number = (singleItem.sku_number == "") ? true : false;
		// 	beneficiaryItemErrors.push(object);
		// });
		
		object.beneficiary_type = (temp_beneficiary.beneficiary_type == "") ? true : false;
		if(temp_beneficiary.beneficiary_type == "person"){
			object.first_name = (temp_beneficiary.first_name == "") ? true : false;
			object.last_name = (temp_beneficiary.last_name == "") ? true : false;
			object.nric = (temp_beneficiary.nric == "") ? true : false;	
			object.charity_name = false;
			object.charity_address = false;
		}
		if(temp_beneficiary.beneficiary_type == "charity"){
			object.first_name = false;
			object.last_name = false;
			object.nric = false;	
			object.charity_name = (temp_beneficiary.charity_name == "") ? true : false;
			object.charity_address = (temp_beneficiary.charity_address == "") ? true : false;
		}
		
		object.beneficiary_checkbox_one_error = (this.state.beneficiary_checkbox_one == false) ? true : false;
		object.beneficiary_checkbox_two_error = (this.state.beneficiary_checkbox_two == false) ? true : false;
		object.beneficiary_checkbox_three_error = (this.state.beneficiary_checkbox_three == false) ? true : false;

		// object.beneficiaryItemErrors = beneficiaryItemErrors;
		this.setState({
			distribution_errors : object
		});
		let errors_number = 0;
		// errors_number = (object.beneficiary_type == true) ? errors_number + 1 : errors_number;
		if(temp_beneficiary.beneficiary_type == "person"){
			errors_number = (object.first_name == true) ? errors_number + 1 : errors_number;
			errors_number = (object.last_name == true) ? errors_number + 1 : errors_number;
			errors_number = (object.nric == true) ? errors_number + 1 : errors_number;
		}
		if(temp_beneficiary.beneficiary_type == "charity"){
			errors_number = (object.charity_name == true) ? errors_number + 1 : errors_number;
			errors_number = (object.charity_address == true) ? errors_number + 1 : errors_number;
		}
		errors_number = (object.allocation_of_state == true) ? errors_number + 1 : errors_number;
		errors_number = (object.address == true) ? errors_number + 1 : errors_number;
		errors_number = (object.beneficiary_checkbox_one_error == true) ? errors_number + 1 : errors_number;
		errors_number = (object.beneficiary_checkbox_two_error == true) ? errors_number + 1 : errors_number;
		errors_number = (object.beneficiary_checkbox_three_error == true) ? errors_number + 1 : errors_number;
		
		// beneficiaryItemErrors.map((singleError,index) => {
		// 	errors_number = (singleError.item_description == true) ? errors_number + 1 : errors_number;
		// 	errors_number = (singleError.sku_number == true) ? errors_number + 1 : errors_number;
		// });

		if(errors_number == 0){
			this.addBeneficiaryObjectToArray();	
			// this.update_tab_state(this.state.active_tab+1); 
		}
		return errors_number;
	}
	distributeCheckboxUpdate = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){
			value = (this.state[name] == true) ? false : true;
		}
		this.setState({
			[name] : value
		})	
	}
	
	checkDistributionOneValidation = () => {
		
		let distribution_one_checkbox_error = (this.state.distribution_one_checkbox == false) ? true : false;
		this.setState({
			distribution_one_errors : { distribution_one_checkbox_error}
		});
		let errors_number = 0;
		errors_number = (this.state.distribution_one_checkbox == false) ? errors_number + 1 : errors_number;
		
		return errors_number;
	}
	setDistributionOneCheckbox = (target) => {
		let {value,name,type} = target;
		if(type == 'checkbox'){

			value = (this.state.distribution_one_checkbox == true) ? false : true;
		}
		this.setState({
			[name] : value
		})	
	}

	/**Distribution End**/

	
	
	/** Common **/
	update_tab_state(value){
		this.setState({
			active_tab: value
		});
	}
	// checkAllValidationFromActiveToRequested(active_tab,requestedTab){
	// 	for(active_tab;active_tab<=requestedTab;active_tab++){
	// 		if(active_tab == 1){
	// 			if(this.checkProfileValidation() > 0){
	// 				this.update_tab_state(active_tab);
	// 				break;
	// 			}
	// 		}else if(active_tab == 2){
	// 			if(this.checkKeyInfoOneValidation() > 0){
	// 				this.update_tab_state(active_tab);
	// 				break;
	// 			}
	// 		}else if(active_tab == 3){
	// 			if(this.checkKeyInfoTwoValidation() > 0){
	// 				this.update_tab_state(active_tab);
	// 				break;
	// 			}
	// 		}else if(active_tab == 4){
	// 			if(this.checkKeyInfoThreeValidation() > 0){
	// 				this.update_tab_state(active_tab);
	// 				break;
	// 			}
	// 		}else if(active_tab == 5){
	// 			this.setState({
	// 				active_distribute_section : 1
	// 			});
	// 			if(this.state.distribution.beneficiaries.length > 0 && this.checkDistributionOneValidation() == 0){
	// 				this.update_tab_state(active_tab);
	// 				break;
	// 			}
	// 		}
	// 	}
	// }

	validateAndGo = (requestedTab) => {
		
		if(requestedTab > this.state.active_tab){
			if(this.state.active_tab == 1){
				if(this.checkProfileValidation() == 0){
					this.update_tab_state(requestedTab);
				}else{
					this.update_tab_state(this.state.active_tab);
				}
			}else if(this.state.active_tab == 2){
				if(this.checkKeyInfoOneValidation() == 0){
					this.update_tab_state(requestedTab);
				}else{
					this.update_tab_state(this.state.active_tab);
				}
			}else if(this.state.active_tab == 3){
				if(this.checkKeyInfoTwoValidation() == 0){
					this.update_tab_state(requestedTab);
				}else{
					this.update_tab_state(this.state.active_tab);
				}
			}else if(this.state.active_tab == 4){
				if(this.checkKeyInfoThreeValidation() == 0){
					this.update_tab_state(requestedTab);
				}else{
					this.update_tab_state(this.state.active_tab);
				}
			}else if(this.state.active_tab == 5){
				this.setState({
					active_distribute_section : 1
				});
				if(this.state.distribution.beneficiaries.length > 0 && this.checkDistributionOneValidation() == 0){
					this.update_tab_state(requestedTab);
				}else{
					this.update_tab_state(this.state.active_tab);
				}
			}
		}else{
			this.update_tab_state(requestedTab);
		}
	}
	/** Common End */

	render(){
		return (
			<section className="profile-section">
				<ProfileHeader/>
				<TabPills update_tab_state={this.update_tab_state.bind(this)} active_tab={this.state.active_tab} validateAndGo={this.validateAndGo}/>
				<TabContents 
					update_tab_state={this.update_tab_state.bind(this)} 
					active_tab={this.state.active_tab}
					setChangedValue = {this.setChangedValue}
					setKeyInfoOneCheckbox = {this.setKeyInfoOneCheckbox}
					setKeyInfoTwoCheckbox = {this.setKeyInfoTwoCheckbox}
					setKeyInfoThreeCheckbox = {this.setKeyInfoThreeCheckbox}
					checkProfileValidation = {this.checkProfileValidation}
					checkKeyInfoOneValidation = {this.checkKeyInfoOneValidation}
					checkKeyInfoTwoValidation = {this.checkKeyInfoTwoValidation}
					checkKeyInfoThreeValidation = {this.checkKeyInfoThreeValidation}
					profile_errors = {this.state.profile_errors}
					addExecutor = {this.addExecutor}
					addSubstituteExecutor = {this.addSubstituteExecutor}
					addGuardian = {this.addGuardian}
					removeExecutor = {this.removeExecutor}
					removeSubstituteExecutor = {this.removeSubstituteExecutor}
					removeGuardian = {this.removeGuardian}
					allState = {this.state}
					setExecutorChangedValue = {this.setExecutorChangedValue}
					setSubstituteExecutorChangedValue = {this.setSubstituteExecutorChangedValue}
					setGuardianChangedValue = {this.setGuardianChangedValue}
					setSubstituteGuardianChangedValue = {this.setSubstituteGuardianChangedValue}
					setBenificiaryInput = {this.setBenificiaryInput}
					addBeneficiaryObjectToArray = {this.addBeneficiaryObjectToArray}
					addCharityObjectToArray = {this.addCharityObjectToArray}
					changeDistributionSection = {this.changeDistributionSection}
					setBeneficiaryType={this.setBeneficiaryType}
					checkDistributionValidation={this.checkDistributionValidation}
					removeBeneficiary={this.removeBeneficiary}
					removeCharity={this.removeCharity}
					distributeCheckboxUpdate={this.distributeCheckboxUpdate}
					setBenificiaryItemInput={this.setBenificiaryItemInput}
					setCharityInput={this.setCharityInput}
					removeBeneficiaryItem={this.removeBeneficiaryItem}
					addItemToTemporaryDistribution = {this.addItemToTemporaryDistribution}
					validateAndGo = {this.validateAndGo}
					goToPreviewPage = {this.goToPreviewPage}
					setDistributionOneCheckbox = {this.setDistributionOneCheckbox}
					handleDateOfBirthChange={this.handleDateOfBirthChange}
				/>
			</section>

		);            
	}
}
 
export default ProfileSection;
