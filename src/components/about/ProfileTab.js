import React, { Component } from "react";
import CountryList from "../CountryList.js";
import DatePicker from "react-datepicker";

 
import "react-datepicker/dist/react-datepicker.css";
class ProfileTab extends Component {
  constructor(props){
    super(props);
  }
  state = {
    startDate: new Date()
  };
  handleChange = date => {
    this.props.handleDateOfBirthChange(date);
    
  };

  render(){
    let props = this.props;
    let classes = (props.active_tab == 1) ? "tab-pane fade show active" : "tab-pane fade" ;
    return (

      <div className={classes} id="profile" role="tabpanel" aria-labelledby="profile">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 offset-2 justify-content-center tab-profile2">
              <h2>Testator Profile (this is you!)</h2>
              <p>This section allows us to ensure that your details are accurately recorded.</p>
              <div className="form-row justify-content-center">
                <div className="col-auto">
                  <label>First Name</label>
                  <input type="text" className={"form-control mb-2 "+(props.profile_errors && props.profile_errors.profile_first_name==true ? 'error_border' : '')} id="inlineFormInput" name="profile_first_name" onChange={(e)=> this.props.setChangedValue(e.target)} />
                </div>
                <div className="col-auto">
                  <label>Last Name</label>
                  <input type="text" className={"form-control mb-2 "+(props.profile_errors && props.profile_errors.profile_last_name==true ? 'error_border' : '')} id="inlineFormInput" name="profile_last_name" onChange={(e)=> this.props.setChangedValue(e.target)} />
                </div>
                <div className="col-auto">
                  <label style={{display:"block"}}>Date of Birth</label>
                  <DatePicker
        selected={this.props.allState.profile_date_of_birth_unformatted}
        onChange={this.handleChange}
        dateFormat={"d MMMM yyyy"}
        className={"form-control mb-2"}
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
      />
                </div>
              </div>
              <div className="justify-content-center tab-profile2-hr"><hr/></div>
              <div className="form-row justify-content-center">
                <div className="col-auto">
                  <label>NRIC (or Passport)</label>
                  <input type="text" className={"form-control mb-2 "+(props.profile_errors && props.profile_errors.profile_nric==true ? 'error_border' : '')} id="inlineFormInput" name="profile_nric" onChange={(e)=> this.props.setChangedValue(e.target)}/>
                </div>
                <div className="col-auto">
                  <label>Address</label>
                  <input id="address" type="text" className={"form-control mb-2 "+(props.profile_errors && props.profile_errors.profile_address==true ? 'error_border' : '')} name="profile_address" onChange={(e)=> this.props.setChangedValue(e.target)}/>
                </div>
              </div>
              <div className="form-row justify-content-center">
                <div className="col-6 new-select">
                  <label>Country of Birth</label>

                  <select className={(props.profile_errors && props.profile_errors.profile_country_of_birth==true ? 'error_border' : '')} name="profile_country_of_birth" onChange={(e)=> this.props.setChangedValue(e.target)}>
                    <CountryList/>
                  </select>
                </div>
                <div className="col-6 new-select">
                  <label>Country of Residence</label>
                  <select className={(props.profile_errors && props.profile_errors.profile_country_of_residence==true ? 'error_border' : '')} name="profile_country_of_residence" onChange={(e)=> this.props.setChangedValue(e.target)}>
                    <CountryList/>
                  </select>
                  
                </div>
                <div className="col-6 new-select2">
                  <label>Gender</label>
                  <select className={(props.profile_errors && props.profile_errors.profile_gender==true ? 'error_border' : '')} name="profile_gender" onChange={(e)=> this.props.setChangedValue(e.target)}>
                    <option value="">Select</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                  
                </div>
                <div className="col-6 new-select2">
                  <label>Marital Status</label>
                  <select className={(props.profile_errors && props.profile_errors.profile_marital_status==true ? 'error_border' : '')} name="profile_marital_status" onChange={(e)=> this.props.setChangedValue(e.target)}>
                    <option value="">Select</option>
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Widowed">Widowed</option>
                    <option value="Separated">Separated</option>
                    <option value="Divorced">Divorced</option>
                  </select>
                  
                </div>
              </div>
              <div className="form-row profile-checkboxdiv">
                <div className="col-8">

                  <input type="checkbox" className={"form-check-input "+(props.profile_errors && props.profile_errors.profile_checkbox==true ? 'error_outline' : '')} id="profile_tab_checkbox" name="profile_checkbox" onClick={(e)=> this.props.setChangedValue(e.target)}/>
                  <label id="profile_checkbox_label" className="form-check-new" htmlFor="profile_tab_checkbox">I have verified my personal particulars to ensure they are accurate.</label>
                </div>
                <div className="col-4">
                  <button type="submit" className="btn btn-primary mb-2" onClick={()=> this.props.validateAndGo(2) }>Next</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );            
  }
}
export default ProfileTab;