import React, { Component } from "react";
class SingleBeneficiary extends Component {
    constructor(props){
        super(props);
    }
    isThereGiftedMesage(){
    	if(this.props.singleBeneficiary.gifted_sum > 0){
    		return (
	    		<p>Gifts of Money: <strong>{this.props.singleBeneficiary.gifted_sum}</strong> SGD</p>
	    	);	
    	}
    	
    }
	render(){
		return (
			<React.Fragment>
				<div className="form-row distribute-text-wrap justify-content-center">
					<div className="col-4 distribute-text">
						<p>{this.props.singleBeneficiary.first_name}, {this.props.singleBeneficiary.last_name}</p>
						<p>{this.props.singleBeneficiary.nric}</p>
					</div>
					<div className="col-6 distribute-text2">
						<p>Distribution of Estate: <strong>{this.props.singleBeneficiary.allocation_of_state_range} %</strong></p>
						<p>Property: <strong>{this.props.singleBeneficiary.address}</strong></p>
						{this.isThereGiftedMesage()}
					</div>
					<div className="col-2 distribute-text-img">
						<img className="cursor_pointer" src={this.props.icon_fifteen} alt="icon15" onClick={()=>this.props.removeBeneficiary(this.props.beneficiary_number)}/>
					</div>
					
				</div>
				<div className="justify-content-center tab-profile3-hr"><hr/></div>
			</React.Fragment>
		);
	}
}
export default SingleBeneficiary;