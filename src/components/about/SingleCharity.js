import React, { Component } from "react";
class SingleCharity extends Component {
    constructor(props){
        super(props);
    }
    isThereGiftedMesage(){
    	if(this.props.singleCharity.charity_gifted_sum > 0){
    		return (
	    		<p>Gifts of Sum: <strong>{this.props.singleCharity.charity_gifted_sum}</strong> SGD</p>
	    	);	
    	}
    	
    }
	render(){
		return (
			<React.Fragment>
				<div className="form-row distribute-text-wrap justify-content-center">
					<div className="col-4 distribute-text">
						<p>{this.props.singleCharity.charity_name}</p>
						<p>{this.props.singleCharity.charity_address}</p>
					</div>
					<div className="col-6 distribute-text2">
						{this.isThereGiftedMesage()}
					</div>
					<div className="col-2 distribute-text-img">
						<img className="cursor_pointer" src={this.props.icon_fifteen} alt="icon15" onClick={()=>this.props.removeCharity(this.props.charity_number)}/>
					</div>
					
				</div>
				<div className="justify-content-center tab-profile3-hr"><hr/></div>
			</React.Fragment>
		);
	}
}
export default SingleCharity;