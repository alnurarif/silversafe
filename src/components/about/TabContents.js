import React, { Component } from "react";
import NextPreviousSection from "./NextPreviousSection.js";
import ProfileTab from "./ProfileTab.js";
import KeyInfoTabOne from "./KeyInfoTabOne.js";
import KeyInfoTabTwo from "./KeyInfoTabTwo.js";
import KeyInfoTabThree from "./KeyInfoTabThree.js";
import Distribution from "./Distribution.js";
import Preview from "./Preview.js";
import tab_right_arrow from "../../images/tab-right-arrow.png";
import tab_left_arrow from "../../images/tab-left-arrow.png";
import tab_icon1 from "../../images/tab-icon1.png";
import tab_icon2 from "../../images/tab-icon2.png";
import tab_icon3 from "../../images/tab-icon3.png";
import icon15 from "../../images/icon15.png";
import icon14 from "../../images/icon14.png";
import icon13 from "../../images/icon13.png";
import icon13_minus from "../../images/icon13_minus.png";
import icon12 from "../../images/icon12.png";
import progress_slider from "../../images/progress-slider.png";
class TabContents extends Component {
  constructor(props){
    super(props);
    this.state = {
      active_tab : this.props.active_tab
    };
  }
  
  render(){
    let props = this.props;
    
    return (
      <div className="tab-content">
        <NextPreviousSection 
          left_arrow={tab_left_arrow} 
          right_arrow={tab_right_arrow} 
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          validateAndGo={this.props.validateAndGo}/>

        <ProfileTab 
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab} 
          setChangedValue={ this.props.setChangedValue }
          checkProfileValidation={this.props.checkProfileValidation}
          profile_errors={this.props.profile_errors}
          validateAndGo={this.props.validateAndGo}
          handleDateOfBirthChange={this.props.handleDateOfBirthChange}
          allState={this.props.allState}/>
        
        <KeyInfoTabOne 
          tab_icon_one={tab_icon1}
          icon_thirteen={icon13} 
          icon_thirteen_minus={icon13_minus} 
          icon_twelve={icon12}  
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          addExecutor={this.props.addExecutor}
          addSubstituteExecutor={this.props.addSubstituteExecutor}
          removeExecutor={this.props.removeExecutor}
          removeSubstituteExecutor = {this.props.removeSubstituteExecutor}
          allState={this.props.allState}
          setExecutorChangedValue={this.props.setExecutorChangedValue}
          setSubstituteExecutorChangedValue = {this.props.setSubstituteExecutorChangedValue}
          checkKeyInfoOneValidation={this.props.checkKeyInfoOneValidation}
          setKeyInfoOneCheckbox={this.props.setKeyInfoOneCheckbox}
          validateAndGo={this.props.validateAndGo}/>
        
        <KeyInfoTabTwo 
          tab_icon_two={tab_icon2}
          icon_twelve={icon12} 
          icon_thirteen={icon13}
          icon_thirteen_minus={icon13_minus}  
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          addGuardian={this.props.addGuardian}
          removeGuardian={this.props.removeGuardian}
          allState={this.props.allState}
          setGuardianChangedValue={this.props.setGuardianChangedValue}
          setSubstituteGuardianChangedValue = {this.props.setSubstituteGuardianChangedValue}
          checkKeyInfoTwoValidation={this.props.checkKeyInfoTwoValidation}
          setKeyInfoTwoCheckbox={this.props.setKeyInfoTwoCheckbox}
          validateAndGo={this.props.validateAndGo}/>
        
        <KeyInfoTabThree 
          tab_icon_three={tab_icon3}
          icon_twelve={icon12} 
          icon_thirteen={icon13}  
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          allState={this.props.allState}
          checkKeyInfoThreeValidation={this.props.checkKeyInfoThreeValidation}
          setKeyInfoThreeCheckbox={this.props.setKeyInfoThreeCheckbox}
          validateAndGo={this.props.validateAndGo}/>

        <Distribution 
          icon_forteen={icon14}
          progress_slider={progress_slider}  
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          icon_thirteen={icon13}
          icon_forteen={icon14}
          icon_fifteen={icon15}
          progress_slider={progress_slider}
          allState={this.props.allState}
          setBenificiaryInput={this.props.setBenificiaryInput}
          addBeneficiaryObjectToArray={this.props.addBeneficiaryObjectToArray}
          addCharityObjectToArray={this.props.addCharityObjectToArray}
          changeDistributionSection={this.props.changeDistributionSection}
          setBeneficiaryType={this.props.setBeneficiaryType}
          checkDistributionValidation={this.props.checkDistributionValidation}
          removeBeneficiary={this.props.removeBeneficiary}
          removeCharity={this.props.removeCharity}
          distributeCheckboxUpdate={this.props.distributeCheckboxUpdate}
          setBenificiaryItemInput={this.props.setBenificiaryItemInput}
          setCharityInput={this.props.setCharityInput}
          removeBeneficiaryItem={this.props.removeBeneficiaryItem}
          addItemToTemporaryDistribution={this.props.addItemToTemporaryDistribution}
          validateAndGo={this.props.validateAndGo}
          goToPreviewPage={this.props.goToPreviewPage}
          setDistributionOneCheckbox = {this.props.setDistributionOneCheckbox}/>
        
        <Preview 
          icon_thirteen={icon13}
          active_tab={props.active_tab} 
          update_tab_state={this.props.update_tab_state} 
          active_tab={this.props.active_tab}
          allState={this.props.allState}/>
      </div>
    );            
  }
}
 
export default TabContents;

