import React, { Component } from "react";
class TabPills extends Component {
  constructor(props){
    super(props);
    this.state = {
      active_tab : this.props.active_tab
    }
  }
  update_tab_state_event(value){
    this.props.update_tab_state(value);
  }
  isProfile(){
    let classes = (this.props.active_tab == 1 ) ? 'nab-link profile-nav active show' : 'nab-link profile-nav' ;
    return (
      <a className={classes}  onClick={() => this.props.validateAndGo(1)}>Profile</a>
    );
  }
  isKeyInfo(){
    let classes = (this.props.active_tab == 2 || this.props.active_tab == 3 || this.props.active_tab == 4) ? 'nab-link profile-nav active show' : 'nab-link profile-nav' ;
    return (
      <a className={classes}  onClick={() => this.props.validateAndGo(2)}>Key Info</a>
    );
  }
  isDistribution(){
    let classes = (this.props.active_tab == 5 ) ? 'nab-link profile-nav active show' : 'nab-link profile-nav' ;
    return (
      <a className={classes}  onClick={() => this.props.validateAndGo(5)}>Distribution</a>
    );
  }
  isPreview(){
    let classes = (this.props.active_tab == 6 ) ? 'nab-link profile-nav active show' : 'nab-link profile-nav' ;
    return (
      <a className={classes}  onClick={() => this.props.validateAndGo(6)}>Preview</a>
    );
  }
  render(){
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-12 profile-tab-ul">
            <ul className="nav nav-tabs justify-content-center">
              <li className="nav-item">
                  {this.isProfile()}
              </li>
              <li className="nav-item">
                  {this.isKeyInfo()}
              </li>
              <li className="nav-item">
                  {this.isDistribution()}
              </li>
              <li className="nav-item">
                  {this.isPreview()}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );            
  }
}
 
export default TabPills;

