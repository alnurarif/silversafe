import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import logo from './logo.svg';
import logonew from './img/brand-logo.png';
import menuicon1 from './img/menu-icon1.png';
import { Button, Modal } from 'react-bootstrap';
import {
  NavLink
} from "react-router-dom";
 
class Header extends Component {
	
	state= {
	key: 1, show: false 
	}
	setKey = (k) => {
	this.setState({
	key: k
})
}

handleClose = () => {
this.setState({
show: false
})
}

handleShow = () => {
this.setState({
show: true
})
}

  render() {
    return (
        <div>
         <div className="mobile-nav" id="mobile-popup-nav">
         <div className="container">
         <div className="row">
         <div id="career-mb-logo" className="col-6 mobile-logo">
		    <a href="/"><img src={logonew} alt="logonew" /></a>
         </div>
         <div className="col-6 mobile-icon">
         <a onClick={this.handleShow}><img src={menuicon1} alt="menuicon1" /></a>
         </div>
         </div>
		 
      <Modal id="new-popup" show={this.state.show} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title><img src={logonew} alt="logonew" /></Modal.Title>
        </Modal.Header>
        <Modal.Body>
				<Nav className="mr-auto">
            <li><NavLink to="/"></NavLink></li>
			<li><NavLink to="/legacy">Legacy Planning</NavLink></li>
            <li><NavLink to="/mortgage-refi">Mortgage Refi</NavLink></li>
            <li><NavLink to="#">Profile</NavLink></li>
			<li><NavLink to="/userportalacc">Get Started</NavLink></li>
			<li><NavLink to="/loginuser">Log In</NavLink></li>
				</Nav>
		</Modal.Body>
      </Modal>
         </div>
         </div>
		 
         <div className="container">
            <div className="row">
               <div id="heading-nav2" className="col-md-8 heading-nav">
			<Navbar expand="lg">
			  <Navbar.Brand href="/"><img src={logonew} className="site-logo" alt="logonew" /></Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  <Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
            <li><NavLink to="/"></NavLink></li>
			<li><NavLink to="/legacy">Legacy Planning</NavLink></li>
            <li><NavLink to="/mortgage-refi">Mortgage Refi</NavLink></li>
            <li><NavLink to="#">Profile</NavLink></li>
				</Nav>
			  </Navbar.Collapse>
			</Navbar>
               </div>
			  <div className="col-md-4 heading-btn">
              <a href="/userportalacc">Get Started</a>
              <a className="last" href="/loginuser">Log In</a>
               </div>
            </div>
         </div>
	  </div>
    );
  }
}
 
export default Header;
