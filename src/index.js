import React from 'react';
import ReactDOM from 'react-dom';
import Main from "./Main";
import './index.css';
import './responsive.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

library.add(faPlus)

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);