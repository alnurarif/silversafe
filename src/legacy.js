import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./header";
import icon5 from './img/icon5.png';
import icon6 from './img/icon6.png';
import icon7 from './img/icon7.png';
import {
  NavLink
} from "react-router-dom";

class legacy extends Component {

  render() {
    return (
      <div>
      <section className="legacy-section">
	   <Header />
         <div className="container">
            <div className="row">
               <div className="col-lg-6 text-legacy">
               <span>SilverSafe</span>
               <h2>Plan your legacy to leave love for your loved ones.</h2>
        <form>
        <label>Services We Provide</label>
          <div className="form-row align-items-center">
            <div className="col-auto">
             <i className="fa fa-user"></i><select>
             <option>Will Writing</option>
             <option>Will Writing</option>
             <option>Will Writing</option>
             </select>
            </div>
            <div className="col-auto">
              <NavLink to="/profile"><button type="submit" className="btn btn-primary mb-2">Get a Draft</button></NavLink>
            </div>
          </div>
        </form>
        <p>By clicking the button you’re confirming that you agree with our Terms and Conditions.</p>
               </div>
            </div>
         </div>
         <div className="container step-wrap">
            <div className="row">
               <div className="col-lg-12 text-step">
               <h2>Allocate your assets in 3 Simple Steps:</h2>
               </div>
            </div>
         </div>
         <div className="container">
            <div className="row">
               <div className="col-lg-4 text-step2">
               <img src={icon5} alt="icon5" />
               <h3>Key in
				profile info</h3>
                <p>Fill up your name and personal particulars to identify yourself as the owner of your will. </p>
                <p>We will not require information that is not relevant to the will.</p>
               </div>
               <div className="col-lg-4 text-step2">
			   <img src={icon6} alt="icon6" />
               <h3>Appoint
				executor(s)</h3>
                <p>Appoint the administrator to your will to enforce the distribution of your estate. Your executor should be familiar with the legal and operational complexities of executing a will. </p>
               </div>
               <div className="col-lg-4 text-step2">
			   <img src={icon7} alt="icon7" />
               <h3>Distribution
				of estate / assets</h3>
                <p>Assign beneficiaries to your will and assign your estate / assets to be given out. You may distribute personal items. </p>
                <p>You may also choose to donate assets to charity. </p>
               </div>
            </div>
         </div>
      </section>
	  
	  
      <section className="plan-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-8 offset-2 text-plan">
               <h2>Select your preferred plan</h2>
               <p>With our customised will writing and custodian services, we can adjust the will writing process according to your needs.</p>
               </div>
            </div>
         </div>
         <div className="container">
            <div className="row">
               <div className="col-lg-4 col-sm-4 text-plan2">
               <h4>Starter</h4>
               <h2>Free</h2>
               <ul>
               <li>1 Basic Will</li>
               <li>Custodian Service</li>
               <li>Advisor as Intermediary</li> 
               <li>1 Round of Edits</li>
               </ul>
               <button type="button">Select Plan</button>
               <a href="#">Learn More</a>
               </div>
			   <div className="col-lg-4 col-sm-4  text-plan2">
               <h4>SilverSafe Plus</h4>
               <h2>$99</h2>
               <ul>
               <li>1 Customised Will </li>
               <li>Custodian Service</li>
               <li>Access to Legal Counsel </li> 
               <li>3 Round of Edits</li>
               </ul>
               <button type="button">Select Plan</button>
               <a href="#">Learn More</a>
               </div>
			   <div className="col-lg-4 col-sm-4 text-plan2">
               <h4>SilverSafe Pro</h4>
               <h2>$150</h2>
               <ul>
               <li>1 Basic Will</li>
               <li>Custodian Service</li>
               <li>Advisor as Intermediary</li> 
               <li>1 Round of Edits</li>
               </ul>
               <button type="button">Select Plan</button>
               <a href="#">Learn More</a>
               </div>
            </div>
         </div>
      </section>
	  
      <footer className="footer-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-3 footer-logo">
               <h3>SilverSafe</h3>
               </div>
               <div className="col-lg-6 footer-nav">
               <ul>
               <li><a href="#">About Us</a></li>
               <li><a href="#">Terms and Conditions</a></li>
               <li><a href="#">Privacy Policy</a></li>
               </ul>
               </div>
               <div className="col-lg-3 footer-social">
               <a href="#"><i className="fa fa-twitter"></i></a>
               <a href="#"><i className="fa fa-linkedin"></i></a>
               <a href="#"><i className="fa fa-google"></i></a>
               </div>
            </div>
         </div>
      </footer>
      <section className="copyright-section">
         <div className="container copyright-wrap">
            <div className="row">
               <div className="col-lg-12 copyright-text">
                  <p>Copyright © 2020 Normn Pte Ltd</p>
               </div>
            </div>
         </div>
      </section>
	  
      </div>
    );
  }
}
 
export default legacy;