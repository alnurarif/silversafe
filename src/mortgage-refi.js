import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import img1 from './img/img1.jpg';
import img2 from './img/img2.jpg';
import icon8 from './img/icon8.png';
import icon9 from './img/icon9.png';
import icon10 from './img/icon10.png';
import icon11 from './img/icon11.png';
import Header from "./header";
import { Tabs, Tab, Button, Modal } from 'react-bootstrap';
import {
  NavLink
} from "react-router-dom";
 
class mortgage extends Component {
	state= {
	key: 1, show: false 
	}
	setKey = (k) => {
	this.setState({
	key: k
})
}

handleClose = () => {
this.setState({
show: false
})
}

handleShow = () => {
this.setState({
show: true
})
}
    render() {
    return (
      <div>
     <header className="header-bg">
	  <Header />
	  </header>
      <section className="mortgage-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-8 offset-2 text-mortgage">
               <h2>Mortgage Refinancing</h2>
               <p>We reduce the interest rates incurred on your mortgage loan 
by aggregating them into collective pools.</p>
               </div>
            </div>
         </div>
         <div className="container">
            <div className="row">
               <div className="col-lg-6 text-mortgage2">
               <h2>FEATURED LOAN</h2>
			   <img src={img1} alt="img1" />
              <div className="mortgage2-overlay">
              <h3>LOAN QUANTUM:</h3>
              <h4>$20,000,000.00</h4>
              <h5>69% Funded</h5>
              <div className="progress">
              <div className="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <h6>Interest rate: <span>0-1%</span></h6>
              <button type="button">Apply Now</button>
              </div>
              <span className="time1">Available time left to apply:</span><span className="time2">2:02:32</span>
               </div>
               <div className="col-lg-6 text-mortgage3">
               <h2>RECOMMENDED FOR YOU</h2>
               <div className="row text-mortgage4">
               <div className="col-5 text-mortgage5">
			   <img src={img2} alt="img2" />
               <div className="mortgage5-overlay">
              <h3>Interest rate:</h3>
              <span>1-2%</span>
              </div>
               </div>
               <div className="col-7 text-mortgage6">
               <h4>$5M Loan Quantum</h4>
               <h5>82.1% Funded</h5>
               <button type="button">Apply Now</button>
               </div>
               </div>
               <div className="row text-mortgage4">
               <div className="col-5 text-mortgage5">
               <img src={img2} alt="img2" />
               <div className="mortgage5-overlay">
              <h3>Interest rate:</h3>
              <span>1-2%</span>
              </div>
               </div>
               <div className="col-7 text-mortgage6">
               <h4>$5M Loan Quantum</h4>
               <h5>82.1% Funded</h5>
               <button type="button">Apply Now</button>
               </div>
               </div>
               <div className="row text-mortgage4">
               <div className="col-5 text-mortgage5">
               <img src={img2} alt="img2" />
               <div className="mortgage5-overlay">
              <h3>Interest rate:</h3>
              <span>1-2%</span>
              </div>
               </div>
               <div className="col-7 text-mortgage6">
               <h4>$5M Loan Quantum</h4>
               <h5>82.1% Funded</h5>
               <button type="button">Apply Now</button>
               </div>
               </div>
               </div>
            </div>
         </div>
         <div className="container">
         <div className="row">
		<div className="col-lg-3 offset-9 mortgage-pagination">
               <nav aria-label="Page navigation example">
              <ul className="pagination text-right">
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true"><i className="fa fa-angle-left"></i></span>
                    <span className="sr-only">Previous</span>
                  </a>
                </li>
                <li className="page-item"><a className="page-link" href="#">1</a></li>
                <li className="page-item"><a className="page-link" href="#">2</a></li>

                <li className="page-item"><a className="page-link" href="#">3</a></li>
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true"><i className="fa fa-angle-right"></i></span>
                    <span className="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
             </div>
         </div>
         </div>
      </section>
      <section className="access-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-6 text-access">
			   <img className="img1" src={icon8} alt="icon8" />
               <h3>Direct Access</h3>
               <p>Speak to our mortgage brokers who can assist you in getting a better loan for your house immediately.</p>
               <button type="button"><img src={icon10} alt="icon10" />Get access</button>
               </div>
               <div className="col-lg-6 text-access">
			   <img className="img1" src={icon9} alt="icon9" />
               <h3>Get the Latest Update</h3>
               <p>Get the latest update on our rates which are updated daily and download a PDF version here.</p>
               <button id="btn1" type="button"><img src={icon11} alt="icon11" />Download Free</button>
               </div>
            </div>
         </div>
      </section>
      <section className="loans-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-8 offset-2 text-loans">
               <h2>Search for affordable loans</h2>
               </div>
            </div>
         </div>
	  <Tabs id="controlled-tab-example"
      activeKey={this.state.key}
      onSelect={(k) => this.setKey(k)}
    >
      <Tab eventKey={1} title="Housing">
		<div className="container">
            <div className="row">
		<div className="col-lg-12 justify-content-center tab-text">
  			<h2>Choose the loan that best suits you</h2>
        <form>
          <div className="form-row justify-content-center">
            <div className="col-auto">
             <input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Min"></input>
            </div>
            <div className="col-auto">
             <input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Max"></input>
            </div>
            <div className="col-12">
            <select>
             <option>Fixed</option>
             <option>Half</option>
             <option>Half 2</option>
             </select>
            </div>
            <div className="col-12">
              <button type="submit" className="btn btn-primary mb-2">Search</button>
            </div>
          </div>
        </form>
               </div>
            </div>
         </div>
      </Tab>
      <Tab eventKey={2} title="Commercial">
		<div className="container">
            <div className="row">
		<div className="col-lg-12 justify-content-center tab-text">
  			<h2>Choose the loan that best suits you</h2>
        <form>
          <div className="form-row justify-content-center">
            <div className="col-auto">
             <input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Min"></input>
            </div>
            <div className="col-auto">
             <input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Max"></input>
            </div>
            <div className="col-12">
            <select>
             <option>Fixed</option>
             <option>Half</option>
             <option>Half 2</option>
             </select>
            </div>
            <div className="col-12">
              <button type="submit" className="btn btn-primary mb-2">Search</button>
            </div>
          </div>
        </form>
               </div>
            </div>
         </div>
      </Tab>
    </Tabs>
      </section>
      <footer className="footer-section2">
         <div className="container">
            <div className="row">
               <div className="col-lg-3 footer-logo">
               <h3>SilverSafe</h3>
               </div>
               <div className="col-lg-6 footer-nav">
               <ul>
			   <li><NavLink to="#">About Us</NavLink></li>
               <li><a href="#">Terms and Conditions</a></li>
               <li><a href="#">Privacy Policy</a></li>
               </ul>
               </div>
               <div className="col-lg-3 footer-social">
               <a href="#"><i className="fa fa-twitter"></i></a>
               <a href="#"><i className="fa fa-linkedin"></i></a>
               <a href="#"><i className="fa fa-google"></i></a>
               </div>
            </div>
         </div>
      </footer>
      <section className="copyright-section2">
         <div className="container copyright-wrap">
            <div className="row">
               <div className="col-lg-12 copyright-text">
                  <p>Copyright © 2020 Normn Pte Ltd</p>
               </div>
            </div>
         </div>
      </section>

      </div>
    );
  }
}
 
export default mortgage;