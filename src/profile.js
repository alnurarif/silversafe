import React, { Component } from "react";
import { Tabs, Tab, Button, Modal } from 'react-bootstrap';
import Header from "./header";
import ProfileSection from "./components/about/ProfileSection.js";
import './css/style.css';
class About extends Component {
  render(){
    return (
      <React.Fragment>
	  <header className="header-bg">
        <Header />
		</header>
        <ProfileSection/>
      <footer className="footer-section">
         <div className="container">
            <div className="row">
               <div className="col-lg-3 footer-logo">
               <h3>SilverSafe</h3>
               </div>
               <div className="col-lg-6 footer-nav">
               <ul>
               <li><a href="#">About Us</a></li>
               <li><a href="#">Terms and Conditions</a></li>
               <li><a href="#">Privacy Policy</a></li>
               </ul>
               </div>
               <div className="col-lg-3 footer-social">
               <a href="#"><i className="fa fa-twitter"></i></a>
               <a href="#"><i className="fa fa-linkedin"></i></a>
               <a href="#"><i className="fa fa-google"></i></a>
               </div>
            </div>
         </div>
      </footer>
      <section className="copyright-section">
         <div className="container copyright-wrap">
            <div className="row">
               <div className="col-lg-12 copyright-text">
                  <p>Copyright © 2020 Normn Pte Ltd</p>
               </div>
            </div>
         </div>
      </section>
      </React.Fragment>
    );  
  }
  
}
 

 
export default About;