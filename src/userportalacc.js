import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.svg';
import logonew from './img/brand-logo.png';
import userportal from './img/userportal-img1.png';
import {
  NavLink
} from "react-router-dom";
 
class userportalacc extends Component {

  render() {
    return (
        <div>
      <section className="user-section">
         <div className="container-fluid user-wrap">
            <div className="row">
               <div className="col-lg-6 user-left">
			   <img src={userportal} alt="userportal" />
               </div>
               <div className="col-lg-6 user-right">
               <div className="col-lg-12 user-contact-input">
               <li><NavLink to="/loginuser"><span>Log in / </span></NavLink> <NavLink to="/userportalacc">Create account</NavLink></li>
      <label>EMAIL</label>
      <div className="form-group required" data-type="text" data-required="true">
      <input type="text" className="form-control" placeholder="Hey there! Don’t leave me blank :)" name="name1" id="name1" />
      </div>
      </div>
	  <div className="col-lg-12 user-contact-input">
      <label>PASSWORD</label>
      <div className="form-group required" data-type="text" data-required="true">
      <input type="password" className="form-control" placeholder="You can set an alphanumeric password here!" name="name1" id="name1" />
      </div>
      </div>
	  <div className="col-lg-12 user-contact-input">
      <label>CONFIRM PASSWORD</label>
      <div className="form-group required" data-type="text" data-required="true">
      <input type="password" className="form-control" placeholder="Just to make sure you didn’t key in typos :)" name="name1" id="name1" />
      </div>
      </div>
      <div className="row">
      <div className="col-6 user-input-checkbox">
      <input type="checkbox" className="form-check-input" id="exampleCheck1" checked />
      <label id="checkboxlavel" className="form-check-label" for="exampleCheck1">Keep me signed in!</label>
      </div>
      <div className="col-6 user-input-link">
      <a href="#">Forgot password?</a>
      </div>
      </div>
      <div className="row">
      <div className="col-6 user-input-btn">
      <button type="submit">Create Account</button>
      </div>
      <div className="col-6 user-input-link2">
      <a href="#">Not a Client?</a>
      </div>
      </div>
               </div>
            </div>
         </div>
      </section>
	  </div>
    );
  }
}
 
export default userportalacc;
